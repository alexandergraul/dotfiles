;;; ~/.config/doom/config.el -*- lexical-binding: t; -*-
(load! "keybindings")
(load! "orgmode")
(load! "agraul-modeline")
(add-to-list 'load-path (concat doom-user-dir "lisp/"))

;;; UI
(defvar agraul/dark-theme 'ef-owl
  "Dark theme for toggling between dark and light flavour.")
(defvar agraul/light-theme 'doom-alabaster
  "Light theme for toggling between dark and light flavour.")

(map! :leader
      (:prefix ("t" . "toggle")
       :desc "Theme dark/light" "t" #'agraul/toggle-theme))

(setq doom-font (font-spec :family "Berkeley Mono" :size 12.0)
      doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 12.0)
      doom-serif-font (font-spec :family "Go Mono" :size 12.0)

      doom-theme agraul/light-theme
      doom-alabaster-comment-style nil
      doom-gruvbox-dark-variant 'hard
      doom-gruvbox-light-variant 'hard
      display-line-numbers-type nil

      isearch-lazy-count t

      rainbow-delimiters-max-face-count 3
      frame-title-format '("%b - Emacs")
      icon-title-format t)

(use-package! ultra-scroll
  :hook (doom-init-ui-hook . ultra-scroll-mode)
  :init (setq scroll-conservatively 101
              scroll-margin 0))

(set-popup-rule! "^\\*Ping"
  :side 'bottom
  :select nil
  :quit 'current
  :modeline nil)

(set-popup-rule! "^\\*dig output"
  :side 'bottom
  :select t
  :quit t
  :modeline nil)

(use-package! page-break-lines
  :hook (doom-first-buffer . global-page-break-lines-mode))

;; Acme (version without syntax highlighting), needs to be loaded to be activated
(use-package! acme-theme
  :config
  (when (eq doom-theme 'acme)
    (setq global-hl-line-modes '())
    (setq! +modeline-height 27))
  (custom-theme-set-faces! 'acme
    `(highlight :background ,(alist-get 'yellow acme-theme-colors-alist))
    `(success :foreground ,(alist-get 'green-dark acme-theme-colors-alist))
    `(doom-modeline-bar :background ,(alist-get 'frost-deep acme-theme-colors-alist))))


;; Modus Theme
(setq modus-themes-slanted-constructs t
      modus-themes-bold-constructs nil)

;; Doom Alabaster Theme
(custom-theme-set-faces! 'doom-alabaster
  `(+org-todo-onhold :foreground ,(doom-color 'fg-alt) :weight bold)
  `(doom-themes-treemacs-root-face :foreground ,(doom-color 'fg) :weight bold)
  `(doom-themes-treemacs-file-face :foreground ,(doom-color 'fg))
  `(aw-leading-char-face
    :foreground ,(doom-color 'red)
    :background ,(doom-color 'modeline-bg))
  `(forge-topic-done :foreground ,(doom-color 'fg-alt))
  `(forge-topic-pending :foreground ,(doom-color 'fg))
  `(forge-issue-open :weight bold)
  `(forge-topic-unread :foreground ,(doom-color 'fg) :slant italic))

;; Disable solaire for some themes
(defun agraul/doom-theme-no-solaire-p ()
  (member doom-theme '(modus-operandi
                       modus-vivendi
                       acme)))

(defadvice! agraul/load-theme-toggle-solaire-conditionally (&rest _)
  :after 'load-theme
  (solaire-global-mode (if (agraul/doom-theme-no-solaire-p)
                           -1
                         +1)))

;; Configure Berkeley Mono ligature groups as defined in
;; https://cdn.berkeleygraphics.com/static/typefaces/berkeley-mono/datasheet/DX-102-10.pdf
;; https://usgraphics.com/static/products/TX-02/datasheet/TX-02-datasheet.a43c0c7f8d8c.pdf
(use-package! ligature
  :hook (doom-init-ui-hook . global-ligature-mode)
  :config
  (ligature-set-ligatures '(text-mode eww-mode) '("ff" "fi" "ffi"))
  (ligature-set-ligatures
   'prog-mode
   '(; Group A
     ".." ".=" "..." "..<" "::" ":::" ":=" "::=" ";;" ";;;" "??" "???"
     ".?" "?." ":?" "?:" "?=" "**" "***" "/*" "*/" "/**"
     ; Group B
     "<-" "->" "-<" ">-" "<--" "-->" "<<-" "->>" "-<<" ">>-" "<-<" ">->"
     "<-|" "|->" "-|" "|-" "||-" "<!--" "<#--" "<=" "=>" ">=" "<==" "==>"
     "<<=" "=>>" "=<<" ">>=" "<=<" ">=>" "<=|" "|=>" "<=>" "<==>" "||="
     "|=" "//=" "/="
     ; Group C
     "<<" ">>" "<<<" ">>>" "<>" "<$" "$>" "<$>" "<+" "+>" "<+>" "<:" ":<"
     "<:<" ">:" ":>" "<~" "~>" "<~>" "<<~" "<~~" "~~>" "~~" "<|" "|>"
     "<|>" "<||" "||>" "<|||" "|||>" "</" "/>" "</>" "<*" "*>" "<*>" ":?>"
     ; Group D
     "#(" "#{" "#[" "]#" "#!" "#?" "#=" "#_" "#_(" "##" "###" "####"
     ; Group E
     "[|" "|]" "[<" ">]" "{!!" "!!}" "{|" "|}" "{{" "}}" "{{--" "--}}"
     "{!--" "//" "///" "!!"
     ; Group F (without "www")
     "@_" "&&" "&&&" "&=" "~@" "++" "+++" "/\\" "\\/" "_|_" "||"
     ; Group G
     "=:" "=:=" "=!=" "==" "===" "=/=" "=~" "~-" "^=" "__" "!=" "!==" "-~"
     "--" "---"
     ; disable diff markers by setting non-existing ligatures, see
     ; https://github.com/mickeynp/ligature.el/issues/40#issuecomment-2587737716
     "<<<<<<<" "=======" "|||||||" ">>>>>>>")))

;; Globally replace lambda with λ
(add-hook 'prog-mode-hook (lambda () (setq-local prettify-symbols-alist '(("lambda" . ?λ)))))
(add-hook 'doom-init-ui-hook #'global-prettify-symbols-mode)

;; Use Berkely Mono for some symbols
(defun agraul/use-berkeley-symbols ()
  (dolist (chars '((#x2032 . #x2033)
                   #x212E
                   ;; Two ranges powerline glyphs
                   (#xE0A0 . #xE0A3)
                   (#xE0B0 . #xE0B3)))
    (set-fontset-font t chars doom-font)))

(add-hook 'doom-init-ui-hook #'agraul/use-berkeley-symbols)

;;; User information
(setq user-mail-address "mail@agraul.de"
      user-full-name "Alexander Graul"
      auth-source-gpg-encrypt-to '("3D01420053971454F57A4C946A86EBDF0E9226D7"))

;;; Editing
(setq undo-limit 80000000
      ;; mouse
      mouse-autoselect-window t
      focus-follows-mouse t
      projectile-project-search-path '("~/src"))

;; Additional modes
(use-package! systemd
  :mode ("\\.service\\|.target\\|.socket" . systemd-mode))
(use-package! feature-mode
  :mode "\\.feature\\'")
(use-package! salt-mode
  :mode "\\.sls\\'")

;; QoL
(after! which-key
  (setq which-key-idle-delay 0.3))

(use-package! smartscan-mode
  :hook (doom-after-init-hook . global-smartscan-mode)
  :config
  (add-hook! '(comint-mode eshell-mode) (smartscan-mode -1)))

(use-package! whole-line-or-region
  :hook (doom-first-input-hook . whole-line-or-region-global-mode))

(add-hook 'doom-after-init-hook #'repeat-mode)

;; Calendar
;; Use german-holidays.el for list of holidays
;; They are included in org-agenda, see orgmode.el
(use-package! german-holidays
  :config
  (setq holiday-general-holidays holiday-german-BY-holidays
        holiday-bahai-holidays nil
        holiday-christian-holidays nil
        holiday-hebrew-holidays nil
        holiday-islamic-holidays nil
        holiday-orental-holidays nil
        calendar-mark-holidays-flag t))

;; openSUSE packaging
(after! projectile
  (add-to-list 'projectile-project-root-files ".osc")
  (add-to-list 'projectile-globally-ignored-directories ".osc"))
(add-to-list 'auto-mode-alist '("/_service\\'" . xml-mode))

(use-package! changes-mode
  :mode ("\\.changes\\'" . changes-mode))

(use-package! osc
  :commands (osc-cmd-status))

(use-package! uyuni-chlog
  :commands (uyuni-chlog-add uyuni-chlog-rm)
  :custom
  (uyuni-chlog-user "agraul"))

;; bug references
(setq bug-reference-bug-regexp "\\(bsc#\\([0-9]+\\)\\)")
(setq bug-reference-url-format "https://bugzilla.suse.com/show_bug.cgi?id=%s")
(add-hook 'prog-mode-hook #'bug-reference-prog-mode)

;;; :completion corfu
(after! corfu
  (setq corfu-auto nil))

;;; :completion vertico
(use-package! vertico-mouse
  :after vertico
  :config
  (vertico-mouse-mode))
(after! vertico
  (setq +vertico-consult-dir-container-executable "podman"))

;;; :checkers syntax
(after! flycheck
  ;; This executable invokes '-m salt-lint --json', needs to point to Python3
  (setq flycheck-salt-lint-executable "/usr/bin/python3"
        ;; See https://github.com/doomemacs/doomemacs/issues/2497#issuecomment-1440885847
        next-error-find-buffer-function #'next-error-buffer-unnavigated-current))

;;; :ui doom-dashboard
(defun agraul/org-roam-open-daily-work-structure ()
  (interactive)
  (require 'org-roam-id)
  (org-roam-id-open "25dad5cc-8492-4a19-a624-b5dabad5265d"))

(setq ;; GNU Emacs logo > Doom Emacs ASCII art
      fancy-splash-image (concat doom-user-dir "splash.png")
      +doom-dashboard-banner-padding '(2 . 2)
      +doom-dashboard-menu-sections
      '(("Open Daily Work Structure"
         :icon (nerd-icons-mdicon "nf-md-timetable" :face 'doom-dashboard-menu-title)
         :action agraul/org-roam-open-daily-work-structure)
        ("Agenda"
         :icon (nerd-icons-octicon "nf-oct-calendar" :face 'doom-dashboard-menu-title)
         :action org-agenda)
        ("Recently opened files"
         :icon (nerd-icons-faicon "nf-fa-file_text" :face 'doom-dashboard-menu-title)
         :action recentf-open-files)
        ("Reload last session"
         :icon (nerd-icons-octicon "nf-oct-history" :face 'doom-dashboard-menu-title)
         :action doom/quickload-session)
        ("Jump to bookmark"
         :icon (nerd-icons-octicon "nf-oct-bookmark" :face 'doom-dashboard-menu-title)
         :action bookmark-jump)))

;;; :ui (modeline +light)
(defun agraul/+modeline-format-icon (icon-set icon label &optional face help-echo voffset)
  "Copy of `+modeline-format-icon' with centered icon"
  (let ((icon-set-fn (pcase icon-set
                       ('octicon #'nerd-icons-octicon)
                       ('faicon #'nerd-icons-faicon)
                       ('codicon #'nerd-icons-codicon)
                       ('material #'nerd-icons-mdicon))))
    (propertize (concat (funcall icon-set-fn
                                 icon
                                 :face face
                                 :height 1.1
                                 :v-adjust (or voffset 0))
                        (propertize label 'face face))
                'help-echo help-echo)))
(advice-add '+modeline-format-icon :override #'agraul/+modeline-format-icon)

;;; :ui indent-guides
(defvar agraul/indent-bars-disabled-modes '(clojure-mode clojurescript-mode emacs-lisp-mode scheme-mode)
  "Major modes that indent-bars are not turned on in.")
(defun agraul/indent-bars-mode-disabled-p ()
  "Check if current major mode is in agraul/indent-bars-disabled-modes."
  (member major-mode agraul/indent-bars-disabled-modes))
(add-to-list '+indent-guides-inhibit-functions #'agraul/indent-bars-mode-disabled-p)
(after! indent-bars
  (setq indent-bars-starting-column nil))

;;; :ui window-select
(after! ace-window
  ;; colemak home row keys: a r s t g | m n e i o
  ;;  8 6 5 4 - - 1 2 3 7
  (setq aw-keys '(?n ?e ?i ?t ?s ?r ?o ?a)
        aw-dispatch-alist '((?k aw-delete-window "Delete Window")
                            (?m aw-swap-window "Swap Window")
                            (?M aw-move-window "Move Window")
                            (?c aw-copy-window "Copy Window")
                            (?b aw-switch-buffer-in-window "Select Buffer")
                            (?l aw-flip-window "Go to last Window")
                            (?u aw-switch-buffer-in-other-window "Switch Buffer Other Window")
                            (?/ aw-split-window-fair "Split Window Fair")
                            (?v aw-split-window-vert "Split Window Vertically")
                            (?h aw-slip-window-horz "Split Window Horizontally")
                            (?. delete-other-windows "Delete Other Windows")
                            (?? aw-show-dispatch-help))))

;;; :editor parinfer
;; .dir-locals.el is easier to edit with parinfer
(add-hook 'lisp-data-mode-hook #'parinfer-rust-mode)

;;; :emacs dired
(after! dired
  (add-to-list 'dired-guess-shell-alist-user '("\\.pcap\\'" "xdg-open"))
  (remove-hook 'dired-mode-hook #'diredfl-mode))

;;; :tools direnv
(after! projectile
  (add-to-list 'projectile-globally-ignored-directories ".direnv"))

;;; :tools docker
(after! docker
  (setq docker-command "podman"))
(after! dockerfile
  (setq dockerfile-mode-command "podman"))

;;; :tools lookup
(defun agraul/dash-docs-read-json-from-url-a (orig-fun &rest args)
  "Advice for dash-docs-read-json-from-url-a.

Reads agraul^forge API token and sets it as \"Authorization\"
header to circumvent Github API rate limiting."
  (let ((url-request-extra-headers (list (cons "Authorization"
                                               (format
                                                "Bearer %s"
                                                (auth-source-pick-first-password
                                                 :host "api.github.com"
                                                 :user "agraul^forge"))))))
    (apply orig-fun args)))
(advice-add 'dash-docs-read-json-from-url :around #'agraul/dash-docs-read-json-from-url-a)

;;; :tools lsp
;; Let's ignore some directories I don't care about
(after! lsp-mode
  (pushnew! lsp-file-watch-ignored-directories
            "[/\\\\]\\.osc\\'"
            "[/\\\\]\\.log\\'"
            "[/\\\\]\\.pytest_cache\\'"
            "[/\\\\]\\.nox\\'"
            "[/\\\\]\\.tools-venvs\\'"
            "[/\\\\]__pycache__\\'")
  (setq lsp-modeline-diagnostics-enable nil
        lsp-imenu-index-function #'lsp-imenu-create-categorized-index))
(after! lsp-ui
  (setq lsp-ui-doc-max-width 100))

(defadvice! agraul/lsp-ui-doc--mv-at-point (width height start-x start-y)
  "Move lsp-ui-doc frame away from current line

Taken from https://github.com/emacs-lsp/lsp-ui/issues/630#issuecomment-866527728"
  :override #'lsp-ui-doc--mv-at-point
  (-let* (((x . y) (--> (or lsp-ui-doc--bounds (bounds-of-thing-at-point 'symbol))
                        (or (posn-x-y (posn-at-point (car it)))
                            (if (< (car it) (window-start))
                                (cons 0 0)
                              (posn-x-y (posn-at-point (1- (window-end))))))))
          (frame-relative-symbol-x (+ start-x x (* (frame-char-width) 2)))
          (frame-relative-symbol-y (+ start-y y))
          (char-height (frame-char-height))
          ;; Make sure the frame is positioned horizontally such that
          ;; it does not go beyond the frame boundaries.
          (frame-x (or (and (<= (frame-outer-width) (+ frame-relative-symbol-x width))
                            (- x (- (+ frame-relative-symbol-x width)
                                    (frame-outer-width))))
                       x))
          (frame-y (+ (or (and (<= height frame-relative-symbol-y)
                               (- y height))
                          (+ y char-height))
                      (if (fboundp 'window-tab-line-height) (window-tab-line-height) 0)))
          (foo (if (flycheck-overlay-errors-at (point))   ;; <------- the skew of the doc window always appears to be 1 line approx
                   (+ 4 (- char-height)) ;; The 4 is maybe related to the doc window border?
                 0)))
    (cons (+ start-x frame-x) (+ start-y frame-y foo))))

;; enable .dir-locals.el entries like
;; ((nil . ((lsp-file-watch-ignored-directories-extra . ("[\////]ignore-this\\'")))))
(defvar agraul/lsp-file-watch-ignored-directories-extra nil
  "Additional ignored directories added to lsp-file-watch-ignored-directories.")
(put 'agraul/lsp-file-watch-ignored-directories-extra 'safe-local-variable #'agraul/seq-of-strings-p)
(defadvice! ++lsp-file-watch-ignored-directories-extra (orig)
  "Add ignore extra directories."
  :around 'lsp-file-watch-ignored-directories
  (append agraul/lsp-file-watch-ignored-directories-extra (funcall orig)))

;;; :tools magit
(after! magit
  (setq magit-todos-depth 2
        magit-repository-directories '(("~/src" . 2))))
(add-hook 'code-review-mode-hook #'doom-mark-buffer-as-real-h)
(after! forge
  (setq forge-owned-accounts '(("agraul"))))

;;; :lang clojure
(after! cider
  (setq cider-font-lock-dynamically nil
        cider-font-lock-reader-conditionals nil))
(after! smartparens
  (sp-local-pair '(clojure-mode 'clojurescript-mode) "#\"" "\""))
;; (add-hook 'clojure-mode-hook (lambda () (add-to-list '+lookup-documentation-functions #'cider-doc)))
(after! clojure-mode
  (set-docsets! '(clojure-mode clojurescript-mode) "Clojure"))

;;; :lang go
(set-eglot-client! 'go-mode '("gopls" "serve"))

;; :lang java
(after! java
  (add-to-list 'projectile-project-root-files "build.xml"))
(after! lsp-java
  (setq lsp-java-vmargs '("-XX:+UseParallelGC"
                          "-XX:GCTimeRatio=4"
                          "-XX:AdaptiveSizePolicyWeight=90"
                          "-Dsun.zip.disableMemoryMapping=true"
                          "-Xmx4G"
                          "-Xms100m")))
;;; :lang
(after! lsp-lua
  (setq lsp-clients-lua-language-server-command "/usr/bin/lua-language-server"))
  ;; TODO: replace with a function that calls the original one when
  ;; lsp-clients-lua-language-server-command is nil
(advice-add 'lsp-clients-lua-language-server-test :override (lambda () t))

;;; :lang plantuml
(after! plantuml-mode
  (setq plantuml-default-exec-mode 'executable))

;;; :lang python
(after! python
  (setq python-indent-def-block-scale 1
        python-prettify-symbols-alist nil))
;; (after! lsp-pyright
;;   (setq lsp-pyright-diagnostic-mode "workspace"))
(set-file-template! 'python-mode :ignore t)

(use-package! sphinx-doc
  :hook (python-mode . sphinx-doc-mode)
  :init
  (map! :localleader
        :map python-mode-map
        "D" #'sphinx-doc))

(after! dap-mode
  (setq dap-python-debugger 'debugpy))

;;; :lang ruby
(after! lsp-solargraph
  (setq lsp-solargraph-use-bundler t))
(when (modulep! :editor file-templates)
  (defadvice! ++projectile-rails-ignore-snippet-for-lib-a (orig-fn)
    "Use ruby-mode's __module file template by returning no snippet."
    :around 'projectile-rails-corresponding-snippet
    (if (string-match "lib/\\(.+\\)\\.rb$" (buffer-file-name))
        nil
      (funcall orig-fn))))

;;; :lang rust
(after! rustic
  (setq lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-cargo-watch-command "clippy"))

;;; :lang sh
;(setq-hook! 'sh-mode-hook indent-tabs-mode t) my team does not approve
;; For RPM, lacks classes for consult-imenu
(setq-hook! 'sh-mode-hook
  imenu-generic-expression
  `(("Package" "^%package *\\(.*\\)" 1)
    ("Description" "^%description *\\(.*\\)" 1)
    ("Prep" "^%prep *\\(.*\\)" 1)
    ("Build" "^%build *\\(.*\\)" 1)
    ("Install" "^%install *\\(.*\\)" 1)
    ("Check" "^%check *\\(.*\\)" 1)
    ("Files" "^%files *\\(.*\\)" 1)
    ("Post" "^%post *\\(.*\\)" 1)
    ("Postun" "^%postun *\\(.*\\)" 1)
    ("Pre" "^%pre +\\(.*\\)" 1) ;; FIXME: could be just %pre, but must not match %prep
    ("Preun" "^%preun *\\(.*\\)" 1)))


;;; :config default
(after! avy
  ;; colemak home row keys: 8 6 4 5 - - 1 2 3 7
  (setq avy-keys '(?n ?e ?i ?s ?t ?r ?i ?a)))


;;;; Other modes / files
;;; Monkey C -- Garmin Connect IQ
(use-package! monkey-c-mode
  :mode ("\\.mc\\'" . monkey-c-mode)
  :config
  (add-to-list 'compilation-error-regexp-alist 'monkeyc)
  (add-to-list 'compilation-error-regexp-alist-alist
               '(monkeyc "^\\(ERROR\\|WARNING\\): [a-zA-Z0-9]+: \\(.*\\.mc\\):\\([[:digit:]]+\\),\\([[:digit:]]+\\):.*"
                 2 3 4)))

;;; Manage VMs
(use-package! mvm
  :commands (mvm-start mvm-stop mvm-show-ip mvm-ssh-eshell mvm-ssh-shell mvm-ssh-dired)
  :defer t
  :config
  (setq mvm-hostname-alist
        (seq-reduce (lambda (vms vm)
                     (push `(,vm . ,(format "%s.tf.local" vm)) vms))
            (seq-filter (lambda (vm)
                          (or (string-prefix-p "50-" vm)
                              (string-prefix-p "43-" vm)
                              (string-prefix-p "test-salt-" vm)))
                        (mvm-list))
            '())))

;;;Postgresql Connection
(use-package! pgmacs
  :commands (pgmacs))

;;; eshell -- REVIEW (:term eshell) ?
(defvar eshell-buffer-name "*eshell*")
(defvar agraul/eshell-acme-style nil
  "Set to t to rename eshell buffers after the CWD.")

(defun agraul/start-eshell (&optional arg)
  "Start eshell in project or current directory.
ARG is passed through to `eshell'"
  (interactive)
  (if agraul/eshell-acme-style
    (let* ((default-directory (or (projectile-project-root) default-directory))
           (eshell-buffer-name (format "*eshell* - %s" (abbreviate-file-name default-directory))))
      (eshell arg))
    (if (projectile-project-root)
        (projectile-run-eshell arg)
      (eshell arg))))

(defun agraul/map-fns-on-args (f1 f2 args)
  "Call F1 on first element in ARGS.
Call F2 on rest of elements in ARGS."
  (unless (null args)
    (let ((filenames (flatten-list args)))
      (funcall f1 (car filenames))
      (when (cdr filenames)
        (mapcar f2 (cdr filenames))))))

(defun eshell/less (&rest files)
  "Alias to `view-file' with one window per file in FILES."
  (agraul/map-fns-on-args #'view-file
                          #'view-file-other-window
                          files))
;;; prompt
(defun agraul/eshell-current-git-branch ()
  "Get current git branch and formats it for the prompt."
  (let ((branch (doom-call-process "git" "symbolic-ref" "-q" "--short" "HEAD")))
    (if (equal (car branch) 0)
        (format "[%s] " (cdr branch))
      (let ((ref (doom-call-process "git" "describe" "--all" "--always" "HEAD")))
        (if (equal (car ref) 0)
            (format "[%s] " (cdr ref))
          " ")))))

(defun agraul/eshell-prompt ()
  "Eshell prompt function.

Depending on the value of agraul/eshell-acme-style, CWD is included in front.
[<git branch>] %"
  (concat (unless agraul/eshell-acme-style
            (propertize (format "%s" (abbreviate-file-name (eshell/pwd))) 'face 'bold))
          (agraul/eshell-current-git-branch)
          "% "))

(after! eshell
  (setq eshell-hist-ignoredups t
        eshell-aliases-file (concat doom-user-dir "eshell-aliases")
        eshell-prompt-function #'agraul/eshell-prompt
        eshell-prompt-regexp "^[^%]* % "
        eshell-visual-subcommands '(("git" "log" "diff" "show")
                                    ("osc" "diff" "linkdiff" "log" "prdiff"
                                           "projdiff" "projectdiff" "rdiff" "lint"
                                           "rpmlint" "rpmlintlog" "less")))
  (add-hook 'eshell-mode-hook #'doom-mark-buffer-as-real-h)
  (add-hook 'eshell-mode-hook #'turn-off-solaire-mode)
  (add-hook 'eshell-mode-hook #'smartparens-mode)
  (add-to-list 'eshell-modules-list 'eshell-elecslash)
  (add-to-list 'eshell-modules-list 'eshell-smart)
  (add-to-list 'eshell-modules-list 'eshell-tramp))

(setq-hook! 'eshell-mode-hook
  imenu-generic-expression
  `(("%" ,(concat eshell-prompt-regexp "\\(.*\\)") 1)))

;; (defun agraul/eshell-update-buffer-name (&rest r)
;;   "Rename buffer based on current working directory.

;; This function looks at `agraul/eshell-acme-style' to determine if
;; it should rename the buffer."
;;   (when agraul/eshell-acme-style
;;     (rename-buffer (format "*eshell* - %s" (abbreviate-file-name (eshell/pwd))) t)))
;; (advice-add 'eshell/cd :after #'agraul/eshell-update-buffer-name)


;;; Misc functions
(defun agraul/kill-buffer-dwim ()
  "Kill Buffer DWIM."
  (interactive)
  (if (and server-process (> (length server-clients) 1))
      (server-edit)
    (call-interactively #'doom/kill-this-buffer-in-all-windows)))

(defun agraul/set-this-file-modes ()
  "Set unix mode for current buffer's file."
  (interactive)
  (unless (and buffer-file-name (file-exists-p buffer-file-name))
    (user-error "Buffer is not visiting any file"))
  (let ((modes (read-file-modes nil buffer-file-name)))
    (set-file-modes buffer-file-name modes)))

(defun agraul/find-file-in-directory-workspace (file-name)
  "Find file in a workspace based on the directory it is in.

FILE-NAME must be an absolute path.

This function is expected to be used from emacsclient, e.g.

$ emacsclient --eval \"(agraul/find-file-in-directory-workspace \\\"$PWD \\\")\""
  (+workspace-switch (agraul/workspace-name-from-directory file-name) t)
  (find-file file-name))

(defun agraul/workspace-name-from-directory (file-name)
  "Create a workspace name for a given file or directory.

If FILE-NAME is a directory, return the last part of the path.
If FILE-NAME is a file, return the last part of the path of the directory it is
in. "
  (format "%s"
   (if (file-directory-p file-name)
       (car (reverse (split-string file-name "[/]" t)))
     (car (reverse (split-string (file-name-directory file-name) "[/]" t))))))

(defun agraul/seq-of-strings-p (sequence)
  "Return t if all elements of SEQUENCE are strings, else nil."
  (seq-every-p #'stringp sequence))

;;; workaround for modeline shown in *dig output*
;;; see https://discord.com/channels/406534637242810369/1278696230335152209/1280106728607842330
(add-hook! '+modeline-global-mode-hook
  (if +modeline-global-mode
      (progn
        (remove-hook 'after-change-major-mode-hook #'+modeline-global-mode-enable-in-buffers)
        (add-hook 'change-major-mode-after-body-hook #'+modeline-global-mode-enable-in-buffers -100))
    (remove-hook 'change-major-mode-after-body-hook #'+modeline-global-mode-enable-in-buffers)))
