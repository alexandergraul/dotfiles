;;; doom-github.el -*- no-byte-compile: t; -*-
(require 'doom-themes)

;;
(defgroup doom-github-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-github-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-github-theme
  :type 'boolean)

(defcustom doom-github-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-github-theme
  :type 'boolean)

(defcustom doom-github-comment-bg doom-github-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-github-theme
  :type 'boolean)

(defcustom doom-github-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-github-theme
  :type '(choice integer boolean))

(def-doom-theme doom-github
  "A light theme inspired by GitHub's syntax highlighting."

  ;; name        default   256       16
  ((bg         '("#ffffff" nil       nil            ))
   (bg-alt     '("#fafbfc" nil       nil            ))
   (base0      '("#fafbfc" "#fafbfc" "white"        ))
   (base1      '("#e7e7e7" "#e7e7e7" "brightblack"  ))
   (base2      '("#dfdfdf" "#dfdfdf" "brightblack"  ))
   (base3      '("#c6c7c7" "#c6c7c7" "brightblack"  ))
   (base4      '("#6a737d" "#6a737d" "brightblack"  ))
   (base5      '("#383a42" "#424242" "brightblack"  ))
   (base6      '("#202328" "#2e2e2e" "brightblack"  ))
   (base7      '("#1c1f24" "#1e1e1e" "brightblack"  ))
   (base8      '("#1b2229" "black"   "black"        ))
   (fg         '("#24292e" "#24292e" "black"        ))
   (fg-alt     '("#c6c7c7" "#c7c7c7" "brightblack"  ))

   (grey       base4)
   (red        '("#d73a49" "#d73a49" "red"          ))
   (orange     '("#e36209" "#e36209" "brightred"    ))
   (green      '("#22863a" "#22863a" "green"        ))
   (teal       '("#34d058" "#34d058" "brightgreen"  ))
   (yellow     '("#f9c513" "#f9c513" "yellow"       ))
   (blue       '("#005cc5" "#005cc5" "brightblue"   ))
   (dark-blue  '("#032f62" "#032f62" "blue"         ))
   (magenta    '("#6f42c1" "#6f42c1" "magenta"      ))
   (violet     '("#c8c8fa" "#c8c8fa" "brightmagenta"))
   (cyan       '("#0184bc" "#0184bc" "brightcyan"   ))
   (dark-cyan  '("#005478" "#005478" "cyan"         ))
   (dark-red   '("#31d28" "#31d28" nil))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   (doom-darken base2 0.1))
   (selection      violet)
   (builtin        blue)
   (comments       (if doom-github-brighter-comments cyan base4))
   (doc-comments   (doom-darken comments 0.15))
   (constants      red)
   (functions      magenta)
   (keywords       red)
   (methods        cyan)
   (operators      blue)
   (type           blue)
   (strings        dark-blue)
   (variables      magenta)
   (numbers        orange)
   (region         `(,(doom-darken (car bg-alt) 0.1) ,@(doom-darken (cdr base0) 0.3)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    yellow)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (-modeline-bright doom-github-brighter-modeline)
   (-modeline-pad
    (when doom-github-padded-modeline
      (if (integerp doom-github-padded-modeline) doom-github-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt (doom-blend violet base4 (if -modeline-bright 0.5 0.2)))

   (modeline-bg
    (if -modeline-bright
        (doom-darken base2 0.05)
      base1))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken base2 0.1)
      base2))
   (modeline-bg-inactive (doom-darken bg 0.1))
   (modeline-bg-inactive-l `(,(doom-darken (car bg-alt) 0.05) ,@(cdr base1))))

  ;; --- extra faces ------------------------
  ((centaur-tabs-unselected :background bg-alt :foreground base4)
   (font-lock-comment-face
    :foreground comments
    :background (if doom-github-comment-bg base0))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments
    :slant 'italic)

   ((line-number &override) :foreground (doom-lighten base4 0.15))
   ((line-number-current-line &override) :foreground base8)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   ;; magit
   ;; (magit-section-highlight :foreground fg)
   ;; (magit-section-heading-selection :foreground fg)
   (magit-filename :foreground fg)
   (magit-blame-heading     :foreground orange :background bg-alt)
   (magit-diff-hunk-heading :background violet)
   (magit-diff-hunk-heading-highlight :foreground bg :background magenta)
   (magit-diff-removed :foreground (doom-darken red 0.2) :background (doom-blend red bg 0.1))
   (magit-diff-removed-highlight :foreground red :background (doom-blend red bg 0.2) :bold bold)
   (git-commit-comment-file :foreground fg)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)

   ;; mu4e-mode

   ;; gnus-mode
   (gnus-header-content :foreground fg)
   (gnus-header-from :bold bold :foreground fg)
   (gnus-cite-1 :foreground dark-blue)
   (gnus-cite-2 :foreground blue)
   (gnus-cite-attribution :inherit 'gnus-cite-1 :bold t)
   (gnus-signature :foreground comments)

   ;; markdown-mode
   (markdown-markup-face     :foreground base5)
   (markdown-header-face     :inherit 'bold :foreground red)
   ((markdown-code-face &override)       :background base1)
   (mmm-default-submode-face :background base1)

   ;; org-mode
   ((outline-1 &override) :foreground dark-blue :bold nil)
   ((outline-2 &override) :foreground blue :bold nil)
   ((outline-3 &override) :foreground fg :bold nil)
   ((outline-4 &override) :foreground (doom-darken green 0.2) :bold nil)
   ((outline-5 &override) :foreground green :bold nil)
   ((outline-6 &override) :foreground violet :bold nil)
   (org-table :foreground fg )
   ((org-block &override) :background base1)
   ((org-block-begin-line &override) :foreground fg :slant 'italic)
   (org-ellipsis :underline nil :background bg     :foreground red)
   ((org-quote &override) :background base1)
   (org-date :foreground red)
   (org-done :foreground fg :bold nil)

   ;; helm
   (helm-candidate-number :background blue :foreground bg)

   ;; web-mode
   (web-mode-current-element-highlight-face :background dark-blue :foreground bg)

   ;; wgrep
   (wgrep-face :background base1)

   ;; ediff
   (ediff-current-diff-A        :foreground red   :background (doom-lighten red 0.8))
   (ediff-current-diff-B        :foreground green :background (doom-lighten green 0.8))
   (ediff-current-diff-C        :foreground blue  :background (doom-lighten blue 0.8))
   (ediff-current-diff-Ancestor :foreground teal  :background (doom-lighten teal 0.8))

   ;; tooltip
   (tooltip :background base1 :foreground fg)

   ;; posframe
   (ivy-posframe               :background base0)

   ;; lsp
   (lsp-ui-doc-background      :background base0)
   (lsp-face-highlight-read    :background (doom-blend yellow bg 0.3))
   (lsp-face-highlight-textual :inherit 'lsp-face-highlight-read)
   (lsp-face-highlight-write   :inherit 'lsp-face-highlight-read)
   )

  ;; --- extra variables ---------------------
  ()
  )

;;; doom-github-theme.el ends here
