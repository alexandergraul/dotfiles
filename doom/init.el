;;; init.el -*- lexical-binding: t; -*-

(doom! :completion
       (corfu +icons)
       (vertico +icons)

       :ui
       doom                  ; what makes DOOM look the way it does
       doom-dashboard        ; a nifty splash screen for Emacs
       hl-todo               ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW tags
       indent-guides         ; highlighted indent columns
       ;; ligatures
       ;; (modeline +light)     ; snazzy, Atom-inspired modeline, plus API
       ;; minimap
       ;; ophints            ; highlight the region an operation acts on
       (popup +defaults)     ; tame sudden yet inevitable temporary windows
       (treemacs +lsp)       ; a project drawer, like neotree but cooler
       (vc-gutter +pretty)   ; vcs diff in the fringe
       ;; vi-tilde-fringe    ; fringe tildes to mark beyond EOB
       ;; window-select         ; visually switch windows
       workspaces            ; tab emulation, persistence & separate workspaces
       zen                   ; distraction-free coding or writing

       :editor
       file-templates        ; auto-snippets for empty files
       ;; fold               ; (nigh) universal code folding
       format                ; automated prettiness
       multiple-cursors      ; editing in many places at once
       parinfer
       ;; rotate-text        ; cycle region at point between text candidates
       snippets              ; my elves . They type so I don't have to
       word-wrap             ; soft wrapping with language-aware indent

       :emacs
       (dired                ; making dired pretty [functional]
        +icons)              ; colorful icons for dired-mode
       (ibuffer +icons)
       ;; electric           ; smarter, keyword-based electric-indent
       ;; undo               ; persistent, smarter undo for your inevitable mistakes
       vc                    ; version-control and Emacs, sitting in a tree

       :term
       shell                 ; another terminals in Emacs

       :checkers
       (syntax +childframe)  ; tasing you for every semicolon you forget
       (spell                ; tasing you for misspelling mispelling
        +flyspell
        +aspell)

       :tools
       (debugger +lsp)       ; FIXME stepping through code, to help you add bugs
       direnv
       docker
       ;;editorconfig        ; let someone else argue about tabs vs spaces
       (eval +overlay)       ; run code, run (also, repls)
       (lookup               ; helps you navigate your code and documentation
        +docsets
        +offline
        +dictionary)
       ;; (lsp +eglot)
       (lsp +peek)
       ;; magit
       (magit +forge)        ; a git porcelain for Emacs
       pdf                   ; pdf enhancements
       terraform
       tree-sitter
       upload                ; map local to remote projects via ssh/ftp

       :lang
       (cc +lsp)             ; C/C++/Obj-C madness
       clojure
       data                  ; config/data formats
       emacs-lisp            ; drown in parentheses
       (go +lsp)             ; the hipster dialect
       (java +lsp +treesitter)
       (javascript +lsp)     ; all(hope(abandon(ye(who(enter(here))))))
       json
       (lua +lsp)
       markdown              ; writing docs for people to ignore
       (org                  ; organize your plain life in plain text
        +dragndrop           ; file drag & drop support
        +roam2
        +journal
        ;;+pandoc            ; pandoc integration into org's exporter
        +present)            ; using Emacs for presentations
       plantuml              ; diagrams for confusing people more
       (python               ; beautiful is better than ugly
        +lsp
        +pyright)
        ;; +poetry)
       (rest +jq)            ; Emacs as a REST client
       rst
       ;; (ruby +lsp)
       (rust +lsp)           ; Fe2O3 . unwrap().unwrap().unwrap().unwrap()
       (scheme +guile)
       sh                    ; she sells (ba|z|fi)sh shells on the C xor
       ;; web                ; the tubes

       :config
       (default +smartparens))
