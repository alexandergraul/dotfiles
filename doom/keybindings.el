;;; ~/.config/doom/keybindings.el -*- lexical-binding: t; -*-

;; After a Doom :config default refactoring, more bindings are behind the +bindings flag.
;; I replicate them here because these bindings replace +bindings.

;; Vertico + Consult + Corfu
(define-key!
  :keymaps +default-minibuffer-maps
  "C-s" #'consult-history)

(map! :when (modulep! :completion corfu)
      :after corfu
      (:map corfu-map
         [remap corfu-insert-separator] #'+corfu/smart-sep-toggle-escape
         "C-S-s" #'+corfu/move-to-minibuffer
         "C-p" #'corfu-previous
         "C-n" #'corfu-next))
(let ((cmds-del
       `(menu-item "Reset completion" corfu-reset
         :filter ,(lambda (cmd)
                    (when (and (>= corfu--index 0)
                               (eq corfu-preview-current 'insert))
                      cmd))))
      (cmds-ret
       `(menu-item "Insert completion DWIM" corfu-insert
         :filter ,(lambda (cmd)
                    (pcase +corfu-want-ret-to-confirm
                      ('nil (corfu-quit) nil)
                      ('t (if (>= corfu--index 0) cmd))
                      ('both (funcall-interactively cmd) nil)
                      ('minibuffer
                       (if (minibufferp nil t)
                           (ignore (funcall-interactively cmd))  ; 'both' behavior
                         (if (>= corfu--index 0) cmd)))  ; 't' behavior
                      (_ cmd)))))
      (cmds-tab
       `(menu-item "Select next candidate or expand/traverse snippet" corfu-next
         :filter (lambda (cmd)
                   (cond
                    ,@(when (modulep! :editor snippets)
                        '(((and +corfu-want-tab-prefer-navigating-snippets
                                (memq (bound-and-true-p yas--active-field-overlay)
                                      (overlays-in (1- (point)) (1+ (point)))))
                           #'yas-next-field-or-maybe-expand)
                          ((and +corfu-want-tab-prefer-expand-snippets
                                (yas-maybe-expand-abbrev-key-filter 'yas-expand))
                           #'yas-expand)))
                    ,@(when (modulep! :lang org)
                        '(((and +corfu-want-tab-prefer-navigating-org-tables
                                (featurep 'org)
                                (org-at-table-p))
                           #'org-table-next-field)))
                    (t cmd)))))
      (cmds-s-tab
       `(menu-item "Select previous candidate or expand/traverse snippet"
         corfu-previous
         :filter (lambda (cmd)
                   (cond
                    ,@(when (modulep! :editor snippets)
                        '(((and +corfu-want-tab-prefer-navigating-snippets
                                (memq (bound-and-true-p yas--active-field-overlay)
                                      (overlays-in (1- (point)) (1+ (point)))))
                           #'yas-prev-field)))
                    ,@(when (modulep! :lang org)
                        '(((and +corfu-want-tab-prefer-navigating-org-tables
                                (featurep 'org)
                                (org-at-table-p))
                           #'org-table-previous-field)))
                    (t cmd))))))
  (map! :when (modulep! :completion corfu)
        :map corfu-map
        [backspace] cmds-del
        "DEL" cmds-del
        :gi [return] cmds-ret
        :gi "RET" cmds-ret
        "S-TAB" cmds-s-tab
        [backtab] cmds-s-tab
        :gi "TAB" cmds-tab
        :gi [tab] cmds-tab))

;; Leader, workspaces & projectile
(setq doom-leader-alt-key "C-c"
      doom-localleader-alt-key "C-c l")
(setq! persp-keymap-prefix (kbd "C-c w"))
(require 'projectile nil t) ; we need its keybinds immediately
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;;; Mark 'org-capture-goto-target autoload to enable binding it below
(autoload 'org-capture-goto-target "org-capture" nil t)

;;; Leader keys
(map! :leader
      :desc "Evaluate line/region"          "e" #'+eval/line-or-region

      (:prefix ("l" . "<localleader>")) ; bound locally
      (:prefix ("!" . "checkers"))      ; bound by flycheck                                ;

      ;;; <leader> c --- code
      (:prefix-map ("c" . "code")
       :desc "Compile"                      "c" #'compile
       :desc "Recompile"                    "C" #'recompile
       :desc "Jump to definition"           "d" #'+lookup/definition
       :desc "Jump to references"           "D" #'+lookup/references
       :desc "Evaluate buffer/region"       "e" #'+eval/buffer-or-region
       :desc "Evaluate & replace region"    "E" #'+eval/region-and-replace
       :desc "Format buffer/region"         "f" #'+format/region-or-buffer
       :desc "Find implementations"         "i" #'+lookup/implementations
       :desc "Jump to documentation"        "k" #'+lookup/documentation
       :desc "Send to repl"                 "s" #'+eval/send-region-to-repl
       :desc "Find type definition"         "t" #'+lookup/type-definition
       :desc "Delete trailing whitespace"   "w" #'delete-trailing-whitespace
       :desc "Delete trailing newlines"     "W" #'doom/delete-trailing-newlines
       :desc "List errors"                  "x" #'+default/diagnostics
       :desc "Rename symbol"                "r" #'agraul/symbol-rename
       (:when (modulep! :tools lsp -eglot)
        :desc "LSP Code actions"            "a" #'lsp-execute-code-action
        :desc "LSP Organize imports"        "o" #'lsp-organize-imports
        :desc "LSP"                         "l" #'+default/lsp-command-map
        :desc "Jump to symbol (workspace)"  "j" #'consult-lsp-symbols
        :desc "Jump to symbol (anywhere)"   "J" (cmd!! #'consult-lsp-symbols 'all-workspaces)
        (:when (modulep! :ui treemacs +lsp)
         :desc "Errors list"                "X" #'lsp-treemacs-errors-list
         :desc "Incoming call hierarchy"    "y" #'lsp-treemacs-call-hierarchy
         :desc "Outgoing call hierarchy"    "Y" (cmd!! #'lsp-treemacs-call-hierarchy t)
         :desc "References tree"            "R" (cmd!! #'lsp-treemacs-references t)
         :desc "Symbols"                    "S" #'lsp-treemacs-symbols))
       (:when (modulep! :tools lsp +eglot)
        :desc "LSP Execute code action"     "a" #'eglot-code-actions
        :desc "Jump to symbol (workspace)"  "j" #'consult-eglot-symbols))

      ;;; <leader> i --- insert
      (:prefix-map ("i" . "insert")
       :desc "Emoji"              "e" #'emoji-search
       :desc "Nerd icon"          "n" #'nerd-icons-insert
       :desc "Current file name"  "f" #'+default/insert-file-path
       :desc "Current file path"  "F" (cmd!! #'+default/insert-file-path t)
       :desc "Snippet"            "s" #'yas-insert-snippet
       :desc "Unicode"            "u" #'insert-char
       :desc "From clipboard"     "y" #'+default/yank-pop)

      ;;; <leader> f --- file
      (:prefix-map ("f" . "file")
       (:when (modulep! :tools editorconfig)
         :desc "Open project editorconfig" "c" #'editorconfig-find-current-editorconfig)
       :desc "Copy this file"              "C" #'doom/copy-this-file
       :desc "Find directory"              "d" #'dired
       :desc "Delete this file"            "D" #'doom/delete-this-file
       :desc "Find file in emacs.d"        "e" #'doom/find-file-in-emacsd
       :desc "Browse emacs.d"              "E" #'doom/browse-in-emacsd
       :desc "Find file"                   "f" #'find-file
       :desc "Find file from here"         "F" #'+default/find-file-under-here
       :desc "Locate file"                 "l" #'locate
       :desc "Rename/move this file"       "m" #'doom/move-this-file
       :desc "Set file modes"              "M" #'agraul/set-this-file-modes
       :desc "Open file externally"        "o" #'agraul/xdg-open-at-point
       :desc "Find file in private config" "p" #'doom/find-file-in-private-config
       :desc "Browse private config"       "P" #'doom/open-private-config
       :desc "Recent files"                "r" #'recentf-open-files
       :desc "Recent project files"        "R" #'projectile-recentf
       :desc "Sudo this file"              "u" #'doom/sudo-this-file
       :desc "Sudo find file"              "U" #'doom/sudo-find-file
       :desc "Yank file path"              "y" #'+default/yank-buffer-path
       :desc "Yank file path from project" "Y" #'+default/yank-buffer-path-relative-to-project
       :desc "Open scratch buffer"         "x" #'doom/open-scratch-buffer
       :desc "Switch to scratch buffer"    "X" #'doom/switch-to-scratch-buffer
       :desc "Find file at point"          "." #'ffap)

      ;;; <leader> r --- remote
      (:when (modulep! :tools upload)
       (:prefix-map ("r" . "remote")
        :desc "Browse remote"              "b" #'ssh-deploy-browse-remote-base-handler
        :desc "Browse relative"            "B" #'ssh-deploy-browse-remote-handler
        :desc "Download remote"            "d" #'ssh-deploy-download-handler
        :desc "Delete local & remote"      "D" #'ssh-deploy-delete-handler
        :desc "Eshell base terminal"       "e" #'ssh-deploy-remote-terminal-eshell-base-handler
        :desc "Eshell relative terminal"   "E" #'ssh-deploy-remote-terminal-eshell-handler
        :desc "Move/rename local & remote" "m" #'ssh-deploy-rename-handler
        :desc "Open this file on remote"   "o" #'ssh-deploy-open-remote-file-handler
        :desc "Run deploy script"          "s" #'ssh-deploy-run-deploy-script-handler
        :desc "Upload local"               "u" #'ssh-deploy-upload-handler
        :desc "Upload local (force)"       "U" #'ssh-deploy-upload-handler-forced
        :desc "Diff local & remote"        "x" #'ssh-deploy-diff-handler
        :desc "Browse remote files"        "." #'ssh-deploy-browse-remote-handler
        :desc "Detect remote changes"      ">" #'ssh-deploy-remote-changes-handler))

      ;;; <leader> s --- search
      (:prefix-map ("s" . "search")
       :desc "Search project for symbol"        "." #'+default/search-project-for-symbol-at-point
       :desc "Search project buffers"           "b" #'consult-line-multi
       :desc "Search all open buffers"          "B" (cmd!! #'consult-line-multi 'all-buffers)
       :desc "Search current directory"         "d" #'+default/search-cwd
       :desc "Search other directory"           "D" #'+default/search-other-cwd
       :desc "Search .emacs.d"                  "e" #'+default/search-emacsd
       :desc "Locate file"                      "f" #'+lookup/file
       :desc "Jump to symbol"                   "i" #'imenu
       :desc "Jump to symbol in open buffers"   "I" #'consult-imenu-multi
       :desc "Open link"                        "l" #'link-hint-open-link
       :desc "Jump to link"                     "L" #'ffap-menu
       :desc "Jump to bookmark"                 "m" #'bookmark-jump
       :desc "Look up online"                   "o" #'+lookup/online
       :desc "Look up online (w/ prompt)"       "O" #'+lookup/online-select
       :desc "Look up in local docsets"         "k" #'+lookup/in-docsets
       :desc "Look up in all docsets"           "K" #'+lookup/in-all-docsets
       :desc "Search project"                   "p" #'+default/search-project
       :desc "Search other project"             "P" #'+default/search-other-project
       :desc "Search buffer"                    "s" #'+default/search-buffer
       :desc "Search buffer for thing at point" "S" #'+vertico/search-symbol-at-point
       :desc "Dictionary"                       "t" #'+lookup/dictionary-definition
       :desc "Thesaurus"                        "T" #'+lookup/synonyms)

      ;;; <leader> n --- notes
      (:prefix-map ("n" . "notes")
       :desc "Search notes for symbol"        "." #'+default/search-notes-for-symbol-at-point
       :desc "Org agenda"                     "a" #'org-agenda
       :desc "Toggle last org-clock"          "c" #'+org/toggle-last-clock
       :desc "Cancel current org-clock"       "C" #'org-clock-cancel
       :desc "Find file in notes"             "f" #'+default/find-in-notes
       :desc "Browse notes"                   "F" #'+default/browse-notes
       :desc "Org store link"                 "l" #'org-store-link
       :desc "Tags search"                    "m" #'org-tags-view
       :desc "Org capture"                    "n" #'org-capture
       :desc "Goto capture"                   "N" #'org-capture-goto-target
       :desc "Active org-clock"               "o" #'org-clock-goto
       :desc "Todo list"                      "t" #'org-todo-list
       :desc "Search notes"                   "s" #'+default/org-notes-search
       :desc "Search org agenda headlines"    "S" #'+default/org-notes-headlines
       :desc "View search"                    "v" #'org-search-view
       :desc "Org export to clipboard"        "y" #'+org/export-to-clipboard
       :desc "Org export to clipboard as RTF" "Y" #'+org/export-to-clipboard-as-rich-text
       (:when (modulep! :lang org +journal)
        (:prefix ("j" . "journal")
         :desc "New Entry"                    "j" #'org-journal-new-entry
         :desc "New Scheduled Entry"          "J" #'org-journal-new-scheduled-entry
         :desc "Search Forever"               "s" #'org-journal-search-forever))
       (:prefix ("r" . "roam")
        :desc "Open random node"              "a" #'org-roam-node-random
        :desc "Find node"                     "f" #'org-roam-node-find
        :desc "Find ref"                      "F" #'org-roam-ref-find
        :desc "Show graph"                    "g" #'org-roam-graph
        :desc "Insert node"                   "i" #'org-roam-node-insert
        :desc "Capture to node"               "n" #'org-roam-capture
        :desc "Toggle roam buffer"            "r" #'org-roam-buffer-toggle
        :desc "Launch roam buffer"            "R" #'org-roam-buffer-display-dedicated
        :desc "Sync database"                 "s" #'org-roam-db-sync))

      ;;; <leader> o --- open
      "o" nil ; we need to unbind it first as Org claims this prefix
      (:prefix-map ("o" . "open")
       :desc "Browser"                       "b"  #'browse-url-of-file
       :desc "Debugger"                      "d"  #'+debugger/start
       :desc "Eshell"                        "e"  #'agraul/start-eshell
       :desc "New frame"                     "f"  #'make-frame
       :desc "REPL"                          "r"  #'+eval/open-repl-other-window
       :desc "REPL (same window)"            "R"  #'+eval/open-repl-same-window
       :desc "Dired"                         "-"  #'dired-jump
       :desc "Org agenda"                    "a"  #'org-agenda
       :desc "Org agenda (today)"            "A"  #'org-agenda-list
       (:when (modulep! :ui treemacs)
        :desc "Project sidebar"              "p" #'+treemacs/toggle
        :desc "Find file in project sidebar" "P" #'treemacs-find-file)
       (:when (modulep! :emacs dired +dirvish)
        :desc "Project sidebar"              "p" #'dirvish-side
        :desc "Find file in project sidebar" "P" #'+dired/dirvish-side-and-follow)
       (:when (modulep! :term shell)
        :desc "Toggle shell popup"           "t" #'+shell/toggle
        :desc "Open shell here"              "T" #'+shell/here)
       (:when (modulep! :term vterm)
        :desc "Toggle vterm popup"           "v" #'+vterm/toggle
        :desc "Open vterm here"              "V" #'+vterm/here)
       (:when (modulep! :term eshell)
        :desc "Toggle eshell popup"          "e" #'+eshell/toggle
        :desc "Open eshell here"             "E" #'+eshell/here)
       (:when (modulep! :tools docker)
        :desc "Docker"                       "D" #'docker))

      ;;; <leader> p --- project
      (:prefix ("p" . "project")
       :desc "Browse project"                   "." #'+default/browse-project
       :desc "Browse other project"             ">" #'doom/browse-in-other-project
       :desc "Find file in other project"       "F" #'doom/find-file-in-other-project
       :desc "Search project"                   "s" #'+default/search-project
       :desc "Open project scratch buffer"      "x" #'doom/open-project-scratch-buffer
       :desc "Switch to project scratch buffer" "X" #'doom/switch-to-project-scratch-buffer
       ;; later expanded by projectile
       (:prefix ("4" . "in other window"))
       (:prefix ("5" . "in other frame")))

      ;;; <leader> q --- quit/restart
      (:prefix-map ("q" . "quit/restart")
       :desc "Restart emacs server"       "d" #'+default/restart-server
       :desc "Delete frame"               "f" #'delete-frame
       :desc "Clear current frame"        "F" #'doom/kill-all-buffers
       :desc "Kill Emacs (and daemon)"    "K" #'save-buffers-kill-emacs
       :desc "Quit Emacs"                 "q" #'kill-emacs
       :desc "Save and quit Emacs"        "Q" #'save-buffers-kill-terminal
       :desc "Quick save current session" "s" #'doom/quicksave-session
       :desc "Restore last session"       "l" #'doom/quickload-session
       :desc "Save session to file"       "S" #'doom/save-session
       :desc "Restore session from file"  "L" #'doom/load-session
       :desc "Restart & restore Emacs"    "r" #'doom/restart-and-restore
       :desc "Restart Emacs"              "R" #'doom/restart)

      ;;; <leader> & --- snippets
      (:prefix-map ("&" . "snippets")
       :desc "New snippet"          "n" #'yas-new-snippet
       :desc "Insert snippet"       "i" #'yas-insert-snippet
       :desc "Find global snippet"  "/" #'yas-visit-snippet-file
       :desc "Reload snippets"      "r" #'yas-reload-all
       :desc "Create Temp Template" "c" #'aya-create
       :desc "Use Temp Template"    "e" #'aya-expand)

      ;;; <leader> t --- toggle
      (:prefix-map ("t" . "toggle")
       :desc "Big mode"              "b" #'doom-big-font-mode
       :desc "Fill Column Indicator" "c" #'global-display-fill-column-indicator-mode
       :desc "Frame fullscreen"      "F" #'toggle-frame-fullscreen
       :desc "Indent style"          "I" #'doom/toggle-indent-style
       :desc "Line numbers"          "l" #'doom/toggle-line-numbers
       :desc "Visible mode"          "v" #'visible-mode
       :desc "Soft line wrapping"    "w" #'+word-wrap-mode
       :desc "Flycheck"              "f" #'flycheck-mode
       :desc "Indent guides"         "i" #'indent-bars-mode
       :desc "org-tree-slide mode"   "p" #'org-tree-slide-mode
       :desc "Read-only mode"        "r" #'read-only-mode
       :desc "Spell checker"         "s" #'flyspell-mode
       :desc "Zen mode"              "z" #'+zen/toggle
       :desc "Zen mode (fullscreen)" "Z" #'+zen/toggle-fullscreen)

      ;;; <leader> g --- git / version control
      (:prefix-map ("g" . "Git")
       :desc "Git revert file"        "R" #'vc-revert
       :desc "Kill link to remote"    "y" #'+vc/browse-at-remote-kill
       :desc "Kill link to homepage"  "Y" #'+vc/browse-at-remote-kill-homepage
       :desc "Git revert hunk"        "r" #'+vc-gutter/revert-hunk
       :desc "Git stage hunk"         "s" #'+vc-gutter/stage-hunk
       :desc "Git time machine"       "t" #'git-timemachine-toggle
       :desc "Jump to next hunk"      "n" #'+vc-gutter/next-hunk
       :desc "Jump to previous hunk"  "p" #'+vc-gutter/previous-hunk
       :desc "Magit dispatch"         "/" #'magit-dispatch
       :desc "Magit file dispatch"    "." #'magit-file-dispatch
       :desc "Forge dispatch"         "'" #'forge-dispatch
       :desc "Magit status"           "g" #'magit-status
       :desc "Magit status here"      "G" #'magit-status-here
       :desc "Magit file delete"      "x" #'magit-file-delete
       :desc "Magit blame"            "B" #'magit-blame-addition
       :desc "Magit clone"            "C" #'magit-clone
       :desc "Magit fetch"            "F" #'magit-fetch
       :desc "Magit buffer log"       "L" #'magit-log-buffer-file
       :desc "Git stage file"         "S" #'magit-stage-buffer-file
       :desc "Git unstage file"       "U" #'magit-unstage-buffer-file
       (:prefix ("f" . "find")
        :desc "Find file"             "f" #'magit-find-file
        :desc "Find gitconfig file"   "g" #'magit-find-git-config-file
        :desc "Find commit"           "c" #'magit-show-commit
        :desc "Find issue"            "i" #'forge-visit-issue
        :desc "Find pull request"     "p" #'forge-visit-pullreq)
       (:prefix ("o" . "open in browser")
        :desc "Browse file or region" "." #'+vc/browse-at-remote
        :desc "Browse homepage"       "h" #'+vc/browse-at-remote-homepage
        :desc "Browse remote"         "r" #'forge-browse-remote
        :desc "Browse commit"         "c" #'forge-browse-commit
        :desc "Browse an issue"       "i" #'forge-browse-issue
        :desc "Browse a pull request" "p" #'forge-browse-pullreq
        :desc "Browse issues"         "I" #'forge-browse-issues
        :desc "Browse pull requests"  "P" #'forge-browse-pullreqs)
       (:prefix ("l" . "list")
        (:when (modulep! :tools gist)
         :desc "List gists"           "g" #'gist-list)
        :desc "List repositories"     "r" #'magit-list-repositories
        :desc "List submodules"       "s" #'magit-list-submodules
        :desc "List issues"           "i" #'forge-list-issues
        :desc "List pull requests"    "p" #'forge-list-pullreqs
        :desc "List notifications"    "n" #'forge-list-notifications)
       (:prefix ("c" . "create")
        :desc "Initialize repo"       "r" #'magit-init
        :desc "Clone repo"            "R" #'magit-clone
        :desc "Commit"                "c" #'magit-commit-create
        :desc "Fixup"                 "f" #'magit-commit-fixup
        :desc "Issue"                 "i" #'forge-create-issue
        :desc "Pull request"          "p" #'forge-create-pullreq))

      ;;; <leader> w --- workspaces
      (:prefix-map ("w" . "workspaces/windows")
       :desc "Display workspaces"     "w" #'+workspace/display
       :desc "Go to workspace"        "." #'+workspace/switch-to
       :desc "Go to other workspace"  "o" #'+workspace/other
       :desc "Create named workspace" "c" #'+workspace/new-named
       :desc "Create workspace"       "C" #'+workspace/new
       :desc "Save workspace"         "S" #'+workspace/save
       :desc "Rename workspace"       "r" #'+workspace/rename
       :desc "Delete workspace"       "k" #'+workspace/kill
       :desc "Delete saved workspace" "K" #'+workspace/delete
       :desc "Go to left workspace"   "p" #'+workspace/switch-left
       :desc "Go to right workspace"  "n" #'+workspace/switch-right
       :desc "Go to 1st workspace"    "1" #'+workspace/switch-to-0
       :desc "Go to 2nd workspace"    "2" #'+workspace/switch-to-1
       :desc "Go to 3rd workspace"    "3" #'+workspace/switch-to-2
       :desc "Go to 4th workspace"    "4" #'+workspace/switch-to-3
       :desc "Go to 5th workspace"    "5" #'+workspace/switch-to-4
       :desc "Go to 6th workspace"    "6" #'+workspace/switch-to-5
       :desc "Go to 7th workspace"    "7" #'+workspace/switch-to-6
       :desc "Go to 8th workspace"    "8" #'+workspace/switch-to-7
       :desc "Go to 9th workspace"    "9" #'+workspace/switch-to-8
       :desc "Go to last workspace"   "0" #'+workspace/switch-to-final
       :desc "Undo window config"     "u" #'winner-undo
       :desc "Redo window config"     "U" #'winner-redo)

      (:when (modulep! :editor multiple-cursors)
       (:prefix-map ("m" . "multiple-cursors")
        :desc "Edit lines"         "l"         #'mc/edit-lines
        :desc "Mark next"          "n"         #'mc/mark-next-like-this
        :desc "Unmark next"        "N"         #'mc/unmark-next-like-this
        :desc "Mark previous"      "p"         #'mc/mark-previous-like-this
        :desc "Unmark previous"    "P"         #'mc/unmark-previous-like-this
        :desc "Mark all"           "t"         #'mc/mark-all-like-this
        :desc "Mark all DWIM"      "m"         #'mc/mark-all-like-this-dwim
        :desc "Edit line endings"  "e"         #'mc/edit-ends-of-lines
        :desc "Edit line starts"   "a"         #'mc/edit-beginnings-of-lines
        :desc "Mark tag"           "s"         #'mc/mark-sgml-tag-pair
        :desc "Mark in defun"      "d"         #'mc/mark-all-like-this-in-defun
        :desc "Add cursor w/mouse" "<mouse-1>" #'mc/add-cursor-on-click)))

;;; Non-leader keys
(map! "C-d"                              #'delete-forward-char
      "C-S-a"                            #'doom/backward-to-bol-or-indent
      "C-S-e"                            #'doom/forward-to-last-non-comment-or-eol
      (:after eshell :map eshell-mode-map
       "M-m"                             #'eshell-bol)
      "C-S-r"                            #'vertico-repeat
      "C-="                              #'er/expand-region
      "C-x \\"                           #'align-regexp
      ;; buffer management
      "C-x b"                            #'switch-to-buffer
      ;; https://github.com/minad/consult/issues/251#issuecomment-942776239
      ;; consult-buffer with initial [Project] narrowing
      "C-x B"                            (cmd! (setq unread-command-events
                                                     (append unread-command-events (list ?p 32)))
                                               (consult-buffer))
      "C-x 4 b"                          #'switch-to-buffer-other-window
      "C-x C-b"                          #'ibuffer
      "C-x k"                            #'agraul/kill-buffer-dwim
       ;; Move to new window after splitting
      "C-x 2"                            (cmd! (split-window-below) (other-window 1))
      "C-x 3"                            (cmd! (split-window-right) (other-window 1))
      "M-o"                              #'other-window
      (:map diff-mode-map
       "M-o"                             #'other-window)
      "C-x P"                            #'+popup/other
      "M-'"                              #'+popup/other
      (:map smartscan-map
        "M-'"                              #'+popup/other)
      "C-`"                              #'+popup/toggle
      "C-~"                              #'+popup/raise

      "C-;"                              #'completion-at-point
      [remap dabbrev-expand]             #'hippie-expand

      ;; For some reason, these are gone in Doom with modules
      ;; (tested in sandbox, exist in C-c C-d but not in C-c C-p)
      ;; However, I can use the opportunity to invert C-<wheel-up> and
      ;; C-M-<wheel-up>:
      ;; Without M -> everywhere, with M -> single window
      [C-wheel-up]                       #'doom/increase-font-size
      [C-wheel-down]                     #'doom/decrease-font-size
      [C-M-wheel-up]                     #'text-scale-increase
      [C-M-wheel-down]                   #'text-scale-decrease
      [C-down-mouse-2] (cmd! (text-scale-set 0))

      ;; Standardizes the behavior of modified RET to match the behavior of
      ;; other editors, particularly Atom, textedit, textmate, and vscode, in
      ;; which ctrl+RET will add a new "item" below the current one and
      ;; cmd+RET (Mac) / meta+RET (elsewhere) will add a new, blank line below
      ;; the current one.

      ;; auto-indent on newline by default
      [remap newline]                    #'newline-and-indent
      ;; insert literal newline
      "S-RET"                            #'+default/newline
      [S-return]                         #'+default/newline
      "C-j"                              #'+default/newline

      ;; Add new item below current (without splitting current line).
      "C-RET"                            #'+default/newline-below
      [C-return]                         #'+default/newline-below
      ;; Add new item above current (without splitting current line)
      "C-S-RET"                          #'+default/newline-above
      [C-S-return]                       #'+default/newline-above

      (:after flycheck :map flycheck-error-list-mode-map
        "C-n"                            #'flycheck-error-list-next-error
        "C-p"                            #'flycheck-error-list-previous-error
        "RET"                            #'flycheck-error-list-goto-error)

      ;;; help
      ;; help map is configured in default/config.el
      ;; swapping man and where-is bindings here
      "C-h w"                            #'man
      "C-h W"                            #'where-is
      (:after help-mode :map help-mode-map
        "o"                              #'link-hint-open-link
        ">"                              #'help-go-forward
        "<"                              #'help-go-back
        "n"                              #'forward-button
        "p"                              #'backward-button)
      (:after helpful :map helpful-mode-map
        "o"                              #'link-hint-open-link)
      (:after apropos :map apropos-mode-map
        "o"                              #'link-hint-open-link
        "n"                              #'forward-button
        "p"                              #'backward-button)
      (:after info :map Info-mode-map
        "o"                              #'link-hint-open-link)

      (:after smartparens :map smartparens-mode-map
        "C-M-a"                          #'sp-beginning-of-sexp
        "C-M-e"                          #'sp-end-of-sexp
        "C-M-f"                          #'sp-forward-sexp
        "C-M-b"                          #'sp-backward-sexp
        "C-M-n"                          #'sp-next-sexp
        "C-M-p"                          #'sp-previous-sexp
        "C-M-u"                          #'sp-up-sexp
        "C-M-d"                          #'sp-down-sexp
        "C-M-k"                          #'sp-kill-sexp
        "C-M-t"                          #'sp-transpose-sexp
        "C-M-<backspace>"                #'sp-splice-sexp)

      (:when (modulep! :ui treemacs)
        "<f9>"                           #'+treemacs/toggle
        "<C-f9>"                         #'treemacs-find-file)

      (:after vertico :map vertico-map
       "C-SPC"                           #'+vertico/embark-preview)

      ;; Search under M-s
      (:prefix "M-s"
       ;; consult-ripgrep?
       "g" #'+default/search-project
       "s" #'+default/search-buffer)
      ;; Go somewhere under M-g
      (:prefix "M-g"
       "O" #'consult-outline))

;; whole-line-or-region turns M-; into a toggle for the line. It's different
;; from comment-line in that it does not move point to the next line.
;; REVIEW: go back to vanilla comment-line that moves point?
(map! :after whole-line-or-region
      "C-x C-;" #'whole-line-or-region-comment-dwim)

;; keybinding that adds a comment and C-x C-; already toggles line-wise.
(with-eval-after-load 'whole-line-or-region
    (keymap-set whole-line-or-region-local-mode-map
            "<remap> <comment-dwim>" nil))

;;; "Free" key bindings:
;; - "M-j": I can use "ESC C-j" or "RET" for the same purposes
;; - "C-z": I have no use for suspend-frame and it's still on "C-x C-z"
