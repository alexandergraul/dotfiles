;;; ~/.config/doom/orgmode.el -*- lexical-binding: t; -*-
(require 's)

(defvar agraul/org-capture-habit-file   "~/org/inbox.org")
(defvar agraul/org-capture-project-dir  "~/org/projects")
(defvar agraul/org-capture-work-file    "~/org/work.org")
(setq +org-capture-emails-file          "~/org/inbox.org"
      +org-capture-todo-file            "~/org/inbox.org"
      org-attach-id-dir                 "~/org/resources/attachment"
      org-roam-dailies-directory        "~/org/resources/daily"
      org-roam-directory                "~/org"
      org-directory                     "~/org")

;; org mode snippets are not needed, org-roam takes care of inserting content.
(set-file-template! "\\.org$"
  :mode 'org-mode
  :ignore t)

(after! org-roam
  (setq org-roam-capture-templates
        '(("r" "resource" plain "%?"
           :target (file+head "resources/%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\n")
           :unnarrowed t))
        org-roam-dailies-capture-templates
        '(("d" "default" entry "* %<%H:%M> %?"
           :target (file+head "%<%Y-%m-%d>.org"
                              "#+title: %<%Y-%m-%d>\n")))
        +org-roam-open-buffer-on-find-file nil))
(after! org-roam-protocol
  (setq org-roam-capture-ref-templates
        '(("r" "ref" plain "%?"
           :target (file+head "resources/${slug}.org" "#+title: ${title}")
           :unnarrowed t))))

(custom-declare-face 'agraul/org-todo-meeting
                     '((t (:inherit (bold font-lock-type-face))))
                     "Face used for MEETING todo")

(after! org
  (add-to-list 'org-modules 'org-habit)
  (setq org-log-into-drawer t
        org-todo-keywords
        '((sequence "TODO(t!)"
           "NEXT(n!)"
           "WAITING(w@)"
           "|"
           "DONE(d!)"
           "CANCELLED(c@)")
          (sequence "PROJECT(p)" "|" "DONE(D!)" "CANCELLED(C@)")
          (sequence "MEETING(m)" "|" "DONE(M)" "CANCELLED(k)"))
        org-todo-keyword-faces
        '(("WAITING"   . +org-todo-onhold)
          ("PROJECT"   . +org-todo-project)
          ("CANCELLED" . +org-todo-cancel)
          ("MEETING"   . agraul/org-todo-meeting))

        org-capture-templates
        `(("t" "Task" entry
           (file +org-capture-todo-file)
           "* TODO %?\n:PROPERTIES:\n:CREATED:\t%U\n:END:\n%i\n%a"
           :empty-lines 1)
          ("h" "New Habit" entry
           (file agraul/org-capture-habit-file)
           "* TODO %?\n:PROPERTIES:\n:STYLE:\thabit\n:CREATED:\t%U\n:END:\n%i"
           :empty-lines 1)
          ("w" "Work")
          ("wp" "Project" entry
           (file+headline agraul/org-capture-work-file "Projects")
           (function agraul/org-capture-template-work-issue-project)
           :empty-lines 1
           :jump-to-captured t)
          ("wt" "Task" entry
           (file +org-capture-todo-file) ;; REVIEW: file to select project?
           (function agraul/org-capture-template-work-issue-task)
           :empty-lines 1
           :jump-to-captured t)
          ("wm" "Meeting" entry
           (file +org-capture-todo-file)
           "* MEETING %?\n:PROPERTIES:\n:CREATED:\t%U\n:END:\n%i\n%a"
           :empty-lines 1))
        org-agenda-files '("tasks.org" "work.org" "habits.org")
        org-tag-persistent-alist
        '(("@computer" . ?c)
          ("@github"   . ?g)
          ("@home"     . ?h)
          ("@office"   . ?O)
          ("@online"   . ?o)
          ("@phone"    . ?p)
          ("ana"       . ?a)
          ("anywhere"  . ?w)
          ("errands"   . ?e)
          ("email"     . ?E)
          ("idea"      . ?i)
          ("meeting"   . ?m)
          ("read"      . ?r)
          ("salt"      . ?s)
          ("tax"       . ?t)
          ("uyuni"     . ?u))

        org-hide-leading-stars nil
        org-indent-mode-turns-on-hiding-stars nil
        org-hide-emphasis-markers t

        org-export-with-toc nil
        org-export-with-email nil
        org-export-with-author nil
        org-export-with-sub-superscripts nil)

  (add-to-list 'org-refile-targets '("someday.org" :maxlevel . 3))
  (pushnew! org-link-abbrev-alist
            '("bsc" . "https://bugzilla.suse.com/show_bug.cgi?id=%s")
            '("jsc" . "https://jira.suse.com/browse/%s")
            '("obs" . "https://build.opensuse.org/%s")
            '("obsproject" . "https://build.opensuse.org/project/show/%s")
            '("obspackage" . "https://build.opensuse.org/package/show/%s")
            '("ibs" . "https://build.suse.de/%s")
            '("ibsproject" . "https://build.suse.de/project/show/%s")
            '("ibspackage" . "https://build.suse.de/package/show/%s")))

(add-hook 'org-mode-hook #'mixed-pitch-mode)

;;; org appear - shows emphasis markers when point surrounded by them
(use-package! org-appear
  :after org
  :hook (org-mode . org-appear-mode)
  :config
  (setq org-appear-autoemphasis t
        org-appear-autolinks t
        org-appear-autosubmarkers t))

;;; org journal
(defun agraul/org-journal-date-format-func (time)
  (concat (when (= (buffer-size) 0)
            (org-id-get-create)
            (format-time-string  "#+title: Weekly Journal No. %V, %Y\n\n"))
          org-journal-date-prefix
          (format-time-string "%A, %d.%m.") time))

(after! org-journal
  (setq org-journal-file-type 'weekly
        org-journal-date-format 'agraul/org-journal-date-format-func
        org-journal-file-format "%Y%m%d_week%V.org"))

;;; org agenda
(after! org-agenda
  (setq org-agenda-prefix-format
        '((agenda . " %i %-20:c%?-12t% s")
          (todo . " %i %-20:c")
          (tags . " %i %-20:c")
          (search . " %i %-20:c"))
        ;; With this on, PROJECT is always dimmed
        org-agenda-dim-blocked-tasks nil
        ;; include e.g. holidays
        org-agenda-include-diary t))

(setq org-agenda-custom-commands
      '(("p" "Project List" todo "PROJECT"
         ((org-agenda-prefix-format '((todo . "  ")))))
        ("n" "Next Actions" todo "NEXT"
         ((org-agenda-skip-function
           '(org-agenda-skip-entry-if 'scheduled 'deadline))))
        ("w" "WAITING Actions" todo "WAITING")
        ("c" . "Context-aware next actions")))

(defun agraul/org-agenda-context-expand-many (ctx-alist)
  "Create custom commands for org-agenda for multiple GTD context lists.

CTX-ALIST consists of (tag . key). Each key will be prefixed with \"c\""
  (mapcar #'agraul/org-agenda-context-expand ctx-alist))

(defun agraul/org-agenda-context-expand (context)
  "Create an org-agenda-custom-commands entry for a GTD context.

The context (tag . key) is turned into an org-agenda command prefixed with
\"c\", matching the TODO state \"NEXT\", and skipping scheduled items and items
with a deadline. org-tag-alist elements can be used as context."
  (list (concat "c" (string (cdr context)))
        'tags
        (concat (car context) "+TODO=\"NEXT\"")
        '((org-agenda-skip-function '(org-agenda-skip-entry-if 'scheduled 'deadline)))))

(after! org
  (appendq! org-agenda-custom-commands
            (agraul/org-agenda-context-expand-many
             org-tag-persistent-alist)
            (list (agraul/org-agenda-context-expand '("ana+@home" . ?A)))))


(map! :localleader
      :map org-mode-map
      (:prefix "l"
       :desc "Link GitHub" "g" #'agraul/org-link-github
       :desc "Link BugZilla" "b" #'agraul/org-link-bugzilla)
      :desc "Turn entry into a PROJECT" "j" #'agraul/org-entry-to-project
      (:prefix ("y". "Copy")
       :desc "Copy current element" "e" (cmd!
                                         (kill-new
                                          (org-element-property
                                           :value (org-element-at-point))))
       :desc "Copy current subtree" "s" #'org-copy-subtree)
      :desc "Emphasize" "E" #'org-emphasize)

(map! :leader
      (:prefix ("n" . "notes")
       :desc "Capture Floating Note" "." #'org-roam-dailies-capture-today
       (:prefix ("g" . "GTD")
        :desc "List active projects" "p" (cmd! (org-agenda nil "p"))
        :desc "List WAITING tasks" "w" (cmd! (org-agenda nil "w"))
        :desc "List NEXT tasks" "n" (cmd! (org-agenda nil "n")))))


(defvar agraul/github-repo-aliases
  '((uyuni . (uyuni-project uyuni))
    (suma  . (SUSE spacewalk))
    (salt  . (saltstack salt)))
  "Alist that maps aliases to Github repositories.")

(defun agraul/org-link-github (reference)
  "Create a orgmode link to a pull request or issue in Github.

The link has a description with the format OWNER/REPO#ID.
REFERENCE can be a URL to a Pull request or issue in Github or a
OWNER/REPO#ID string. In the latter case the OWNER/REPO part of
the input string can be a key in \`agraul/github-repo-aliases'."
  (interactive "sGitHub Issue/Pull Request: ")
  (insert (agraul/build-org-link-github reference)))

(defun agraul/build-org-link-github (reference)
  ;; full URL
  (if (string-match "^https?://github.com/\\([[:alnum:]-_.]+\\)/\\([[:alnum:]-_.]+\\)/\\(?:issues\\|pull\\)/\\([[:digit:]]+\\)"  reference)
    (agraul/format-org-link-gh
     (match-string 1 reference)
     (match-string 2 reference)
     (match-string 3 reference))
    (let ((args (split-string reference "[#/]")))
      ;; shortcut (e.g. suma#123)
      (if (eq (safe-length args) 2)
        (agraul/format-org-link-gh
         (car (alist-get (intern-soft (car args)) agraul/github-repo-aliases))
         (cadr (alist-get (intern-soft (car args)) agraul/github-repo-aliases))
         (cadr args))
        ;; owner/repo#id
        (if (eq (safe-length args) 3)
            (agraul/format-org-link-gh (car args) (cadr args) (car (cddr args)))
          nil)))))

(defun agraul/format-org-link-gh (project repo id)
  "Format org-link to a Github issue.

The link uses owner/repo#number for the text."
  (format "[[%s][%s/%s#%s]]"
          (format "https://github.com/%s/%s/issues/%s" project repo id)
          project repo id))

(defun agraul/org-link-bugzilla (bug-ref)
  "Create a orgmode link to a Bugzilla Bug given an ID or URL."
  (interactive "sBug ID/URL: ")
  (insert (agraul/build-org-link-bugzilla bug-ref)))

(defun agraul/build-org-link-bugzilla (bug-ref)
  (let ((bug-id (if (string-match
                       "^https://bugzilla.\\(?:suse.com\\|opensuse.org\\)/.*id=\\([[:digit:]]+\\)"
                       bug-ref)
                    (match-string 1 bug-ref)
                  bug-ref)))
       (format "[[https://bugzilla.suse.com/show_bug.cgi?id=%s][bsc#%s]]"
               bug-id bug-id)))


(defun agraul/org-entry-to-project ()
  "Turn the org entry at point into a PROJECT."
  (interactive)
  (org-todo "PROJECT")
  (let ((headline-text (org-get-heading t t t t)))
    (org-edit-headline (concat headline-text " [/]")))
  (org-set-property "CATEGORY" (org-read-property-value "CATEGORY")))

(defun agraul/org-capture-template-work-issue-project (&optional issue-title)
  "org-capture template for Github issues at work (Project)."
  (interactive)
  (let* ((issue-title (or issue-title (read-string "Issue title: ")))
         (parts (agraul/split-github-issue-title issue-title))
         (bug-id (alist-get 'bug parts))
         (title-text (alist-get 'text parts))
         (issue-num (alist-get 'num parts)))
    (concat "* PROJECT " title-text " [/]"
            "\n:PROPERTIES:"
            "\n:CATEGORY: " (if bug-id (format "bsc%s" bug-id) (read-string "Slug: "))
            "\n:END:"
            "\nIssue: " (agraul/format-org-link-gh "SUSE" "spacewalk" issue-num)
            (when bug-id
              (format "\nBug Report: %s" (agraul/build-org-link-bugzilla bug-id)))
            "\n")))

(defun agraul/org-capture-template-work-issue-task (&optional issue-title)
  "org-capture template for Github issues at work (single task)."
  (interactive)
  (let* ((issue-title (or issue-title (read-string "Issue title: ")))
         (parts (agraul/split-github-issue-title issue-title))
         (title-text (alist-get 'text parts))
         (issue-num (alist-get 'num parts)))
    (concat "* TODO " title-text
            "\nIssue: " (agraul/format-org-link-gh "SUSE" "spacewalk" issue-num)
            "\n")))

(defun agraul/split-github-issue-title (title)
  "Split Github issue title into text and issue number.

If the text contains a bug id, the part containing the bug id is
stripped from the text and the bug id is returned separately.

The text is truncated to 90 characters.

Examples:
1. Input:
Research: How to package Salt 3008.x
#25395

Output:
((text . \"Research: How to package Salt 3008.x\")
 (num . 25395))
2. Input:
Bug 1231605 - L3: Adding a SLE Micro 5.5 client with the regular salt stack fails with: AttributeError: \'str\' object has no attribute \'get\'
#25514
Output:
((text . \"L3: Adding a SLE Micro 5.5 client with the regular salt stack fails with: AttributeErro...\" )
 (num . 25514)
 (bug . 1231605))
"
  (let* ((regexp "\\([[:alnum:]:. '\"()]+\\)\n?#\\([[:digit:]]+\\)")
         (bug-regexp (concat "\\(?:Bug \\)?\\([[:digit:]]+\\) - " regexp)))
   (if (string-match bug-regexp title)
       `((bug . ,(match-string 1 title))
         (text . ,(s-truncate 90 (match-string 2 title)))
         (num . ,(match-string 3 title)))
       (string-match regexp title)
       `((text . ,(s-truncate 90 (match-string 1 title)))
         (num . ,(match-string 2 title))))))

(defun agraul/count-org-projects ()
  (length (org-map-entries t "/+PROJECT" 'tree)))

;; From https://github.com/alphapapa/unpackaged.el/tree/d352e743fdb7ae419585714036618a34d152fbf2#ensure-blank-lines-between-headings-and-before-contents
(defun unpackaged/org-fix-blank-lines (&optional prefix)
  "Ensure that blank lines exist between headings and between headings and their contents.
With prefix, operate on whole buffer. Ensures that blank lines
exist after each headings's drawers."
  (interactive "P")
  (org-map-entries (lambda ()
                     (org-with-wide-buffer
                      ;; `org-map-entries' narrows the buffer, which prevents us from seeing
                      ;; newlines before the current heading, so we do this part widened.
                      (while (not (looking-back "\n\n" nil))
                        ;; Insert blank lines before heading.
                        (insert "\n")))
                     (let ((end (org-entry-end-position)))
                       ;; Insert blank lines before entry content
                       (forward-line)
                       (while (and (org-at-planning-p)
                                   (< (point) (point-max)))
                         ;; Skip planning lines
                         (forward-line))
                       (while (re-search-forward org-drawer-regexp end t)
                         ;; Skip drawers. You might think that `org-at-drawer-p' would suffice, but
                         ;; for some reason it doesn't work correctly when operating on hidden text.
                         ;; This works, taken from `org-agenda-get-some-entry-text'.
                         (re-search-forward "^[ \t]*:END:.*\n?" end t)
                         (goto-char (match-end 0)))
                       (unless (or (= (point) (point-max))
                                   (org-at-heading-p)
                                   (looking-at-p "\n"))
                         (insert "\n"))))
                   t (if prefix
                         nil
                       'tree)))
