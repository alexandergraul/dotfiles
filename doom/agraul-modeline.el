;;; doom/agraul-modeline.el -*- lexical-binding: t; -*-
;;; Design ideas
;; Always show:
;; - buffer name
;; - major mode
;; - Read-only and buffer-modified indicator
;; - TRAMP / remote indicator
;;
;; Additionally in source code buffers:
;; - project name
;; - git branch
;; - flycheck indicator (when there are errors or warnings)
;;
;; Temporarily show:
;; - macro recording indicator
;;
;; Active vs Inactive window:
;; The active window should stand out and I use two methods for that:
;; 1. different faces
;; 2. Hiding information on inactive windows (cursor position)
;;
;;; Code
(defgroup agraul-modeline nil
  "My custom modeline."
  :group 'mode-line)

(defcustom agraul-modeline-major-mode-icons t
  "Boolean to enable major mode icons in modeline."
  :type 'boolean
  :group 'agraul-modeline)

;;;Helpers
(defun agraul-modeline--allow-segments (segments)
  (dolist (s segments)
    (put (intern (format "agraul-modeline-%s" s)) 'risky-local-variable t)))

(defun agraul-modeline--fill (reserve)
  "Fill modeline leaving RESERVE characters on right side."
  (propertize " " 'display `((space :align-to (- (+ right right-fringe right-margin)
                                                 ,reserve)))))

(defun agraul-modeline--codicon (codicon-name text face)
  (concat (nerd-icons-codicon
           codicon-name
           :face face
           :height 0.9
           :v-adjust 0.1)
          (propertize text 'face face)))

(defun agraul-modeline--major-mode-icon ()
  "Like following nerd-icons--icon-info-for-buffer with custom height."
  (if (and (buffer-file-name) (nerd-icons-auto-mode-match?))
      (nerd-icons-icon-for-file
       (file-name-nondirectory (buffer-file-name))
       :height 0.9
       :v-adjust 0.1
       :face 'shadow)
      (nerd-icons-icon-for-mode
       major-mode
       :height 0.9
       :v-adjust 0.1
       :face 'shadow)))

;;; Keyboard Macro Recording
;;; FIXME: does not work!
(defvar-local agraul-modeline-macro-recording
    '(:eval
      (when (and (mode-line-window-selected-p) defining-kbd-macro)
        (propertize "Macro" 'face 'mode-line-highlight))))

(agraul-modeline--allow-segments '(macro-recording))

;;; Buffer Identity
(defvar-local agraul-modeline-buffer-name
    '(:eval
      (propertize (buffer-name)
                  'face (if (mode-line-window-selected-p)
                            'mode-line-buffer-id
                          'mode-line-inactive)))
    "Mode line segment to display buffer name.")

(defvar-local agraul-modeline-buffer-status
    '(:eval
      (list
       " "
       (if (buffer-modified-p)
           (nerd-icons-codicon "nf-cod-circle_small_filled" :height 0.6 :v-adjust 0.2)
         " ")
       " "
       (if buffer-read-only
           (propertize "RO" 'face '(bold error))
         "  ")
       " "
       (when (and buffer-file-name (file-remote-p buffer-file-name))
           (nerd-icons-codicon "nf-cod-globe" :height 0.8 :v-adjust 0.2))))
  "Mode line segment to display buffer state.

Shows indicators for modified, read-only and remote-file.")

(defvar-local agraul-modeline-project-name
    '(:eval
      (let ((pname (doom-project-name)))
        (unless (string= "-" pname)
          (concat (nerd-icons-octicon
                   "nf-oct-file_directory"
                   :height 0.9
                   :v-adjust 0.1)
                  (propertize (format " %s" pname) 'display '(space-width 0.2))
                  " "))))
  "Mode line segment to display project name.")

(agraul-modeline--allow-segments
 '(buffer-name
   buffer-status
   project-name))

;;; Cursor Position
(defvar-local agraul-modeline-cursor-position
 '(:eval (when (mode-line-window-selected-p) "L%l:C%c"))
 "Mode line segment to display cursor position in active window.")
(agraul-modeline--allow-segments
 '(cursor-position))

;;; Major Mode
(defun agraul-modeline--major-mode-name ()
  "Return capitalized `major-mode'."
  (capitalize (symbol-name major-mode)))

(defvar-local agraul-modeline-major-mode
    '(:eval
      (list
        ;; REVIEW use icons? defcustom? downsize it?
        (when agraul-modeline-major-mode-icons
          agraul-modeline-major-mode-icon)
        (propertize (agraul-modeline--major-mode-name))))
  "Mode line segment to display major mode.")

(defvar-local agraul-modeline-major-mode-icon
  '(:eval
    (when-let ((icon (agraul-modeline--major-mode-icon)))
      (concat icon (propertize " " 'display '(space-width 0.2))))))

(agraul-modeline--allow-segments
 '(major-mode major-mode-icon))

;;;VC Status
(defvar-local agraul-modeline-vc-status
    '(:eval
      (when vc-mode
        ;; FIXME: truncate branch to max ~12
        (let* ((branch (cadr (split-string (string-trim vc-mode) "^[A-Za-z]+[-:]+")))
               (short-branch (if (<= (length branch) 12)
                                 branch
                               (concat (substring branch 0 12) "..."))))
          (format "%s " short-branch))))
  "Mode line segment to display version control status.")

(agraul-modeline--allow-segments '(vc-status))

;;;Flycheck
;; REVIEW: I want to move to built-in flymake

;; FIXME: DRY
(defun agraul-modeline-flycheck-count-error ()
  (let-alist (flycheck-count-errors flycheck-current-errors)
   .error))
(defun agraul-modeline-flycheck-count-warning ()
  (let-alist (flycheck-count-errors flycheck-current-errors)
   .warning))
(defun agraul-modeline-flycheck-count-info ()
  (let-alist (flycheck-count-errors flycheck-current-errors)
    .info))

(defun agraul-modeline-flycheck-error ()
  (if-let ((count (agraul-modeline-flycheck-count-error)))
      (agraul-modeline--codicon "nf-cod-error" (format "󠀠󠀠%s" (number-to-string count)) 'error)
    "   "))

(defun agraul-modeline-flycheck-warning ()
  (if-let ((count (agraul-modeline-flycheck-count-warning)))
      (agraul-modeline--codicon "nf-cod-warning"(format "󠀠󠀠%s" (number-to-string count)) 'warning)
    "   "))

(defun agraul-modeline-flycheck-info ()
  (if-let ((count (agraul-modeline-flycheck-count-info)))
      (agraul-modeline--codicon "nf-cod-info" (format "󠀠󠀠%s" (number-to-string count)) 'success)
    "   "))

;; FIXME: causes misalignments
(defvar-local agraul-modeline-flycheck-status
  '(:eval
    (when (and (mode-line-window-selected-p) (bound-and-true-p flycheck-mode))
      (list
       '(:eval (agraul-modeline-flycheck-error))
       '(:eval (agraul-modeline-flycheck-warning))
       '(:eval (agraul-modeline-flycheck-info)))))
  "Mode line segment to display flycheck status.")

(agraul-modeline--allow-segments '(flycheck-status))
;;; Putting it together
(defvar agraul-modeline-right
  '(""
    agraul-modeline-project-name
    agraul-modeline-vc-status
    agraul-modeline-major-mode)
  "Segments that make up right side of mode line. ")

(defvar agraul-modeline-left
  '(""
    agraul-modeline-kbd-macro
    agraul-modeline-buffer-name
    agraul-modeline-buffer-status
    " "
    agraul-modeline-cursor-position
    " "
    agraul-modeline-flycheck-status))

;; TODO: split per type of buffer?
;; e.g. code, shell, status, pdfview
(setq-default mode-line-format
              (list
               "%e"
               ;; top & bottom padding (uses TAG SPACE)
               '((:propertize "󠀠󠀠󠀠󠀠" display (raise +0.2))
                 (:propertize "󠀠󠀠󠀠󠀠"  display (raise -0.2)))
               agraul-modeline-left
               ;; REVIEW Emacs30: Look into 'mode-line-format-right-align
               '(:eval (agraul-modeline--fill
                        (1+             ; leave 1 space at end
                         (string-width
                          (format-mode-line agraul-modeline-right)))))
               agraul-modeline-right))

;; (kill-local-variable 'mode-line-format)
