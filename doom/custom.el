(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ignored-local-variable-values
   '((ag/lsp-file-watch-ignored-directories-extra "[/\\\\]branding" "[/\\\\]contrib" "[/\\\\]documentation" "[/\\\\]java" "[/\\\\]schema" "[/\\\\]scripts" "[/\\\\]search-server" "[/\\\\]selinux" "[/\\\\]susemanager-branding-.*" "[/\\\\]susemanager-proxy" "[/\\\\]testsuite" "[/\\\\]web")))
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(safe-local-variable-values
   '((projectile-project-test-prefix . "test_")
     (python-pytest-executable . "pyt")
     (agraul/lsp-file-watch-ignored-directories-extra "[/\\\\]branding" "[/\\\\]contrib" "[/\\\\]documentation" "[/\\\\]java" "[/\\\\]schema" "[/\\\\]scripts" "[/\\\\]search-server" "[/\\\\]selinux" "[/\\\\]susemanager-branding-.*" "[/\\\\]susemanager-proxy" "[/\\\\]testsuite" "[/\\\\]web")
     (eval org-content 2))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ts-fold-replacement-face ((t (:foreground unspecified :box nil :inherit font-lock-comment-face :weight light)))))
(put 'scroll-left 'disabled nil)
