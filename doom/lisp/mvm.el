;;; ~/src/dotfiles/doom/mvm.el -*- lexical-binding: t; -*-
;;; mvm.el: manage virtual machines through `virsh'

(defvar mvm-connection nil
  "Libvirt connection URI.
If nil, $LIBVIRT_DEFAULT_URI or \"qemu:///system\" is used.")

(defvar mvm-hostname-alist nil
  "Alist with VM identifiers as keys and hostnames as values.

This variable is used for hostname lookup. The values are not verified.

Example:
\(setq mvm-hostname-alist \'((\"foo\" . \"foo.lab\"))")

;; TODO learn about macros, this was inspired by
;; https://github.com/jsynacek/emacs-virt-manager/blob/master/virt-manager.el
(defmacro mvm--virsh (out &rest args)
  "Call `virsh' and print its output to OUT.
ARGS are passed to `virsh'.

Example:
\(mvm--virsh t \"list\" \"--all\")"
  `(let ((exitcode
          (call-process "virsh" nil ,out nil "-c" (mvm--connection) ,@args)))
     (when (< 0 exitcode)
       (error "Command virsh %s failed with code %d" (string-join ,@args) exitcode))
     exitcode))

(defun mvm--connection ()
  "Find the correct libvirt URI."
  (or mvm-connection
      (getenv "LIBVIRT_DEFAULT_URI")
      "qemu:///system"))


;;; helper functions
(defun mvm--delete-header ()
  "Delete `virsh' header from output.

Warning: This function operates on the current buffer!"
  (goto-char (point-min))
  (delete-line)
  (delete-line))

(defun mvm--remove-cidr-suffix (ip)
  "Remove CIDR suffix of IP.

Example:
\(mvm--remove-cidr-suffix \"192.168.1.10/24\")
\"192.168.1.10\""
  (car (string-split ip "/")))

(defun mvm--state-matches-p (vm state)
  "Return t if a VM's state equals STATE."
  (string= (symbol-name state) (cadr vm)))

(defun mvm--complete (&optional filter-state)
  "Completing read for VM list.

FILTER-STATE (symbol), if non nil, is used filter the list of VMs
shown in the prompt."
  (completing-read "VM: " (mvm-list filter-state)))

(defun mvm--ssh-call (ip-or-hostname func &optional dir)
  "Call FUNC over via a TRAMP SSH connection to IP-OR-HOSTNAME.

If dir is non-nil, use it instead of /."
  (let ((default-directory (format "/ssh:root@%s:%s"
                                   ip-or-hostname
                                   (or dir "/"))))
    (funcall func)))

;;; virsh calls
(defun mvm--list-all ()
  "List all VMs using \\'virsh list --all\\'."
  (seq-filter (lambda (e) (not (seq-empty-p (car e))))
              (seq-map (lambda (line)
                         (cons (car (cdr (split-string line)))
                               (cdr (cdr (split-string line)))))
                       (split-string
                        (with-temp-buffer
                          (mvm--virsh t "list" "--all")
                          (mvm--delete-header)
                          (while (re-search-forward "shut off" nil t) ; TODO improve state parsing, maybe symbols?
                            (replace-match "shutoff" nil t))
                          (buffer-string))
                        "\n"))))

(defun mvm--domifaddr (vm)
  "Show \\'virsh domifaddr\\' for a given VM."
  (split-string
   (with-temp-buffer
     (mvm--virsh t "domifaddr" vm)
     (mvm--delete-header)
     (buffer-string))))

(defun mvm--get-ip (vm)
  "Read IP address for a VM."
  (mvm--remove-cidr-suffix (cadddr (mvm--domifaddr vm))))

(defun mvm--get-hostname (vm)
  "Read the hostname of VM from \"mvm-hostname-alist\"."
  (alist-get vm mvm-hostname-alist nil nil 'equal))

;;; user-friendly functions and commands
;;;###autoload
(defun mvm-list (&optional filter-state)
  "List names of VMs.

If FILTER-STATE is non-nil, use it to filter VM matching the state.
E.g. \\(mvm-list \\'running\\) will list all running VMs."

  (seq-map 'car (seq-filter (lambda (vm)
                              (if filter-state
                                  (mvm--state-matches-p vm filter-state)
                                t))
                            (mvm--list-all))))

;;;###autoload
(defun mvm-show-ip (&optional vm)
  "Show IP address of VM."
  (interactive)
  (let ((vm (or vm
                (mvm--complete 'running))))
    (message "%s: %s"
             vm
             (mvm--get-ip vm))))

;;;###autoload
(defun mvm-ssh-shell ()
  "Select a running VM and create a shell session over SSH."
  (interactive)
  (let ((vm (mvm--complete 'running)))
    (mvm--ssh-call (or (mvm--get-hostname vm)(mvm--get-ip vm))
                   #'shell "/root")))

;;;###autoload
(defun mvm-ssh-eshell ()
  "Select a running VM and open Eshell in /root over TRAMP."
  (interactive)
  (let ((vm (mvm--complete 'running)))
    (mvm--ssh-call (or (mvm--get-hostname vm)(mvm--get-ip vm))
                   #'eshell "/root")))

;;;###autoload
(defun mvm-ssh-dired ()
  "Select a running VM and start Dired over SSH."
  (interactive)
  (let ((vm (mvm--complete 'running)))
    (mvm--ssh-call (or (mvm--get-hostname vm)(mvm--get-ip vm))
                   (lambda () (dired ".")))))

;;;###autoload
(defun mvm-start ()
  "Start VM."
  (interactive)
  (mvm--virsh nil "start" (mvm--complete 'shutoff)))

;;;###autoload
(defun mvm-stop (arg)
  "Stop VM.

Set ARG to force poweroff instead of shutdown."
  (interactive "P")
  (mvm--virsh nil (if arg "poweroff" "shutdown") (mvm--complete 'running)))

;;;###autoload
(defun mvm-restart ()
  "Restart VM."
  (let ((vm (mvm--complete 'running)))
    (mvm--virsh nil "shutdown" vm)
    ;; FIXME: wait for shutdown
    (mvm--virsh nil "start" vm)))

;;; playground / testing
;;; (and 'nil) to disable evaluation when loading the file
(and 'nil
     (mvm-list)
     (mvm-list 'shutoff)
     (mvm-list 'running)
     (mvm--domifaddr "MicroOS-Desktop")
     (mvm-show-ip "MicroOS-Desktop")
     (let ((mvm-connection "qemu:///session"))
       (mvm-list 'shutoff)))

(provide 'mvm)
;;; mvm.el ends here
