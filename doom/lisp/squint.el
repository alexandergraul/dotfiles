;;; ../../src/dotfiles/doom/squint.el -*- lexical-binding: t; -*-
;;; Coding along https://arialdomartini.github.io/emacs-zoom

(defvar squint-heights
  '((office . 120)
    (laptop . 150)
    (presentation . 170)))

(defun squint--get-height-from-label (label)
  (alist-get (intern label) squint-heights))

(defun squint--set-height (height)
  (set-face-attribute 'default nil :height height))

(defun squint--get-labels (heights)
  (mapcar (lambda (pair) (symbol-name (car pair)))
          heights))

(let ((previous-height (face-attribute 'default :height)))
  (defun squint--reset-height ()
    (message "Reset height to %s" previous-height)
    (squint--set-height previous-height))

  (defun squint ()
    (interactive)
    (consult--read
     squint-heights
     :prompt "Pick squint level: "
     :require-match t
     :state (lambda (action candidate)
              (pcase action
                ('preview (squint--set-height (squint--get-height-from-label candidate)))
                ('return (when (null candidate)
                           (squint--reset-height))))))))

(provide 'squint)
