;;; ../../src/dotfiles/doom/monkey-c-mode.el -*- lexical-binding: t; -*-
;;; Code:
;;;

(defconst monkeyc-keywords
  '("and"        "as"       "break"    "catch"   "case"
    "class"      "const"    "continue" "default" "do"
    "else"       "enum"     "extends"  "false"   "finally"
    "for"        "function" "has"      "hidden"  "if"
    "instanceof" "me"       "module"   "NaN"     "native"
    "new"        "null"     "or"       "private" "protected"
    "public"     "return"   "self"     "static"  "switch"
    "throw"      "true"     "try"      "using"   "var"
    "while")
  "All keywords in Monkey C. Used for font locking.")

(defconst monkeyc-font-lock-keywords-1
  '(("\\_<function\\_>\s+\\([[:word:]]+\\)\s*(.*)\s*{" 1 font-lock-function-name-face)
    ("\\_<class\\_>\s+\\([[:word:]]+\\)\s*{" 1 font-lock-type-face)
    ("\\_<module\\_>\s+\\([[:word:]]+\\)\s*{" 1 font-lock-type-face))
  "Minimum level decoration: function, module and class declarations.")

(defconst monkeyc-font-lock-keywords-2
  (append monkeyc-font-lock-keywords-1
          (list (regexp-opt monkeyc-keywords 'symbols)))
  "Medium level decoration: everything in -1 plus keywords.")

(defconst monkeyc-font-lock-keywords-3 '()
  "Max level decoration.

Everything in -3 plus user-defined constants, variables, etc.")


;;;###autoload
(define-derived-mode monkey-c-mode c-mode "Monkey C Mode"
  "Major mode for editing Monkey C."
  (setq-local comment-start "//"
              comment-end ""

              font-lock-defaults '((monkeyc-font-lock-keywords-1
                                    monkeyc-font-lock-keywords-1
                                    monkeyc-font-lock-keywords-1
                                    monkeyc-font-lock-keywords-1))))


(provide 'monkey-c-mode)
;;; monkey-c-mode.el ends here
