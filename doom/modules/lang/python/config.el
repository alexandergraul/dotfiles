;;; lang/python/config.el -*- lexical-binding: t; -*-

(after! projectile
  (pushnew! projectile-project-root-files
            "pyproject.toml"
            "requirements.txt"
            "setup.py"
            "setup.cfg"))

(use-package! python
  :interpreter ("python3" . python-mode)
  :mode ("[./]flake8\\'" . conf-mode)
  :mode ("\\.wsgi\\'" . python-mode)
  :init
  (when (modulep! +lsp)
   (add-hook 'python-mode-local-vars-hook #'lsp! 'append)
   (set-eglot-client! 'python-mode '("pyright-langserver" "--stdio")))
  (when (modulep! +tree-sitter)
    (add-hook 'python-mode-local-vars-hook #'tree-sitter! 'append))
  :config
  (set-repl-handler! 'python-mode #'+python/open-repl
    :persist t
    :send-region #'python-shell-send-region
    :send-buffer #'python-shell-send-buffer)
  (set-docsets! '(python-mode inferior-python-mode) "Python 3")
  (setq python-shell-interpreter "python3")
  (setq-hook! 'python-mode-hook
    tab-width python-indent-offset
    fill-column 88)

  (add-hook! 'python-mode-hook
    (defun +python-use-correct-flycheck-executables-h ()
      "Use the correct Python executables for Flycheck."
      (let ((executable python-shell-interpreter))
        (save-excursion
          (goto-char (point-min))
          (save-match-data
            (when (or (looking-at "#!/usr/bin/env \\(python[^ \n]+\\)")
                      (looking-at "#! ?\\([^ \n]+/python[^ \n]+\\)"))
              (setq executable (substring-no-properties (match-string 1))))))
        ;; Try to compile using the appropriate version of Python for
        ;; the file.
        (setq-local flycheck-python-pycompile-executable executable)
        ;; We might be running inside a virtualenv, in which case the
        ;; modules won't be available. But calling the executables
        ;; directly will work.
        (setq-local flycheck-python-pylint-executable "pylint")
        (setq-local flycheck-python-flake8-executable "flake8"))))
  ;; REVIEW: Does this work as intended?
  (when (modulep! :tools debugger +lsp)
    (after! dap-mode
      (setq dap-python-debugger 'debugpy)))

  (when (modulep! :completion vertico)
    (after! consult-imenu
      ;; REVIEW: Find a way that works with different providers (pyright, pylsp, no LSP)
      ;; this currently works with lsp-mode + lsp-pyright
      ;; when (eq lsp-imenu-index-function #'lsp-imenu-create-categorized-index)
      (add-to-list 'consult-imenu-config
                   '(python-mode
                     :types
                     ((?f "Functions" font-lock-function-name-face)
                      (?m "Methods"   font-lock-function-name-face)
                      (?C "Constants" font-lock-constant-face)
                      (?c "Classes"   font-lock-type-face)
                      (?F "Fields"    font-lock-variable-name-face)
                      (?v "Variables" font-lock-variable-name-face)))))))

(use-package! pyimport
  :defer t
  :init
  (map! :after python
        :map python-mode-map
        :localleader
        (:prefix
         ("i" . "imports")
         :desc "Insert missing imports" "i" #'pyimport-insert-missing
         :desc "Remove unused imports"  "R" #'pyimport-remove-unused
         :desc "Optimize imports"       "o" #'+python/optimize-imports)))

(use-package! py-isort
  :defer t
  :init
  (map! :after python
        :map python-mode-map
        :localleader
        (:prefix ("i" . "imports")
          :desc "Sort imports"      "s" #'py-isort-buffer
          :desc "Sort region"       "r" #'py-isort-region)))

(use-package! python-pytest
  :commands python-pytest-dispatch
  :init
  (map! :after python
        :localleader
        :map python-mode-map
        :prefix ("t" . "test")
        "a" #'python-pytest
        "f" #'python-pytest-file-dwim
        "F" #'python-pytest-file
        "t" #'python-pytest-run-def-or-class-at-point-dwim
        "T" #'python-pytest-run-def-or-class-at-point
        "r" #'python-pytest-repeat
        "p" #'python-pytest-dispatch))

(use-package! poetry
  :when (modulep! +poetry)
  :after python
  :init
  (setq poetry-tracking-strategy 'switch-buffer)
  (add-hook 'python-mode-hook #'poetry-tracking-mode))

(eval-when! (and (modulep! +lsp)
                 (modulep! +pyright)
                 (not (modulep! :tools lsp :eglot)))
  (use-package! lsp-pyright
    :after lsp-mode))
