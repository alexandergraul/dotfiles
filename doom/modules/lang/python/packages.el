;; -*- no-byte-compile: t; -*-
;;; lang/python/packages.el

(when (and (modulep! +lsp)
           (modulep! +pyright)
           (not (modulep! :tools lsp +eglot)))
  (package! lsp-pyright :pin "327edcea22b27b8ea133aad678123f6d177e247e"))

(when (modulep! +poetry)
  (package! poetry :pin "1dff0d4a51ea8aff5f6ce97b154ea799902639ad"))

(package! python-pytest :pin "dcdaec6fe203f08bda0f5ee1931370dfd075a4ff")
(package! pyimport :pin "4398ce8dd64fa0f685f4bf8683a35087649346d3")
(package! py-isort :pin "e67306f459c47c53a65604e4eea88a3914596560")
