{
    "layer": "top", // Waybar at top layer
    "position": "top",
    "height": 30, // Waybar height (to be removed for auto height)
    "modules-left": ["sway/workspaces", "sway/mode"],
    "modules-center": ["clock"],
    "modules-right": ["idle_inhibitor", "pulseaudio", "network#vpn", "network",
                      "battery", "tray"],
    // Modules configuration
    "sway/workspaces": {
        "disable-scroll": true,
        "all-outputs": false,
        "format": "{icon}",
        "format-icons": {
            "1": "Terminal",
            "2": "Editor",
            "3": "WWW",
            "4": "Chat",
            "5": "Misc1",
            "6": "Misc2",
            "7": "Misc3",
            "8": "Misc4",
            "9": "Music",
            "10": "Passwords"
        }
    },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "sway/window": {
        "tooltip": false
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        "format": "{:%H:%M %a, %d.%m.%y}",
        "interval": 15
    },
    "cpu": {
        "format": "{usage:3}% ",
        "tooltip": false
    },
    "memory": {
        "format": "{:3}% "
    },
    "temperature": {
        // "thermal-zone": 2,
        // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
        "critical-threshold": 80,
        // "format-critical": "{temperatureC}°C {icon}",
        // "format": "{temperatureC}°C {icon}",
        "format": "{temperatureC}°C",
        "format-icons": ["", "", ""]
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            // "good": 95,
            "warning": 25,
            "critical": 15
        },
        "format": "{capacity:3}% {icon}",
        "format-charging": "{capacity:3}% ",
        "format-plugged": "{capacity:3}% ",
        "format-alt": "{time} {icon}",
        "format-icons": ["", "", "", "", ""]
    },
    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-linked": "{ifname} (No IP)",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ifname}: {ipaddr}/{cidr}"
    },
    "network#vpn": {
        "interface": "tun*",
        "format-ethernet": "VPN IP: {ipaddr}/{cidr} ",
        "format-linked": "{ifname} (No IP)",
        "format-disconnected": "VPN off",
        "format-alt": "{ifname}: {ipaddr}/{cidr}"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} {format_source}",
        "format-muted": "0%  {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphones": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click": "pavucontrol"
    }
}
